import React, { Component } from 'react' // Import
import Container from 'react-bootstrap/Container'
import Header, { Item, Mempelai, Divider, Slider } from '../components/main'
import { Helm } from '../components/header'
import { Form, Row, Col, Alert } from 'react-bootstrap'
import { cap } from '../params'
import '../style/style.css'
import '../style/sty.scss'
import music from '../assets/music/selly.aac'
import mask from '../assets/img/dinny/mask.svg'
import distance from '../assets/img/dinny/distance.svg'
import salaman from '../assets/img/dinny/salaman.svg'
import logoig from '../assets/img/dinny/logoig.svg'
import cuci from '../assets/img/selly/cuci.svg'
import path from '../assets/img/selly/path1.svg'
import bunga6 from '../assets/img/bunga6.png'
import AOS from 'aos';
import { gambar } from '../params'
import post from '../params/post'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import logo from '../assets/img/logo.ico'
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';

let cmain = '#cfcfcf'
let mainfont = '#cfcfcf'
let black = 'rgb(38,38,38)'
export default class Halo extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef()
    this.myRef = React.createRef()
    this.nama = React.createRef()
    this.alamat = React.createRef()
    this.pesan = React.createRef()
    this.state = {
      acara: [],
      days: '00',
      minutes: '00',
      hours: '00',
      seconds: '00',
      hide: true,
      hadir: true,
      err: [],
      submitted: '',
      sesi: 2
    }
  }
  componentDidMount() {
    AOS.init({
      // initialise with other settings
      duration: 2000
    });
    var countDownDate = new Date("12/11/2020").getTime();
    // Update the count down every 1 second
    var x = setInterval(() => {
      // Get today's date and time
      var now = new Date().getTime();
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      this.setState({
        days: days, hours: hours, minutes: minutes, seconds: seconds
      })
    }, 1000);
  }

  play = () => {
    AOS.refresh()
    var snd = new Audio(music);
    snd.type = 'audio/aac';
    snd.play();
    this.setState({ hide: false })
    setTimeout(() => {
      var elmnt = document.getElementById('top');
      elmnt.scrollIntoView();
    }, 1000)
  }

  useQuery = () => {
    return new URLSearchParams(this.props.location.search);
  }
  handleSubmit = async () => {
    let err = []
    let local = localStorage.getItem('pesan')
    if (this.nama.current.value == "") {
      err.push('Nama tidak Boleh Kosong')
    }
    if (this.pesan.current.value == "") {
      err.push('Pesan tidak Boleh Kosong')
    }
    if (err.length == 0) {
      confirmAlert({
        message: local ? `Kami mendeteksi bahwa anda telah mengirimkan pesan \" ${local} \", apakah anda ingin mengirim pesan lagi?` : 'Yakin untuk Mengirim Pesan?',
        buttons: [
          {
            label: 'Yes',
            onClick: async () => {
              let send = await post(
                ` dari: "${this.nama.current.value}", hadir: "", jumlahhadir: "", kepada: "selly-arief", pesan:"${this.pesan.current.value}",alamat: ""`
              )
            if (send.status == 200) {
                this.setState({ submitted: true })
                localStorage.setItem('pesan', this.pesan.current.value)
                this.nama.current.value = ''
                this.pesan.current.value = ''
            }else{
                err.push('Koneksi Gagal')
            }
            }
          },
          {
            label: 'No',
            onClick: () => { }
          }
        ]
      });
    } else {
      this.setState({ err: err, submitted: false })
    }


  }
  render() {
    let { hadir, days, hours, minutes, seconds, hide, submitted, err, sesi } = this.state
    let slide = ["	https://i.ibb.co/52WkvfS/IMG-8746.jpg	",
   
    "	https://i.ibb.co/P1Y8qb4/Processed-with-VSCO-with-preset.jpg	",
    "	https://i.ibb.co/ZM2jRBr/cetak-besar-1.jpg	",
    "	https://i.ibb.co/4KVy7Qz/cetak-besar-2.jpg	",
    "	https://i.ibb.co/frDhRGM/Processed-with-VSCO-with-preset.jpg	",
    ]
    let slider = []
    slide.map(v => {
      slider.push(gambar(v, 95,'auto&func=fit&bg_img_fit=1&bg_opacity=0.75&w=720&h=520'))
    })
    let query = this.useQuery().get('name');
    query = query ? cap(query) : ''

    return (
      <>
        <Helm
          title='Undanganku - Selly & Arief'
          desc="undangan digital berbasis website untuk berbagai kebutuhan acara"
          logo={logo}
          img={'https://i.ibb.co/4KVy7Qz/cetak-besar-2.jpg'}
          url='https://undanganku.me/selly-arief'
        />

        <div id='gold5' style={{
          backgroundImage: `url(${'https://i.ibb.co/gmz96rb/Background-Putih.jpg'})`,
          backgroundSize: 'cover', backgroundPosition: 'center',
          
        }}>
          <div className='w-100' style={{
            overflow: 'hidden', maxWidth: '100vw',
            backgroundColor: 'transparent'
          }}>
            <Container fluid id='g3-header' className='position-relative' style={{
              backgroundImage: `url('${gambar('https://i.ibb.co/sRdnrtF/Modal.jpg',
              95, 'auto&func=fit&bg_img_fit=1&bg_opacity=0.75&w=1200&h=900')}')`
            }}>
                <Item><p className='poppins'>Undangan Pernikahan</p>
              </Item>
              <div style={{
                fontSize: '3rem',
                fontFamily: 'Alex Brush, cursive',
                lineHeight: 1
              }}
              >
                <Item>
                  Selly
            </Item>
                <Item>
                  &
            </Item>
                <Item>
                  Arief
            </Item>
            <Item><p className='poppins pt-2' style={{fontSize:'24px'}}> 11 . 12 . 2020</p>
              </Item>

                <Item>
                  {query ? (
                    <h2 className={`col-md-4 poppins text-center pt-5 pt-sm-3`}
                      style={{ fontSize: '16px' }}> Kepada Bapak/Ibu/Saudara/i :<br /><br/>
                      <span className='pt-4' style={{fontSize:'22px'}}>
                        {query}
                      </span>
                    </h2>

                  ) : false}</Item>
              </div>
             
              {/* <Item>
                <Col xs={12} md={4} className='m-2 m-md-0 '>
                  <img className='img-fluid w-100 p-2'
                    src={gambar('https://i.ibb.co/j8SSP5D/Logo.png')} data-aos="fade-left" />
                </Col>
              </Item> */}

              <Row className={`justify-content-center `}>
                <div onClick={() => { this.play() }}
                  className={`btn col-md-4 button roboto-slab text-center ${hide ? 'show' : 'hide'}`}
                  style={{ color:"white" }}>
                  Open Invitation
                  </div>
              </Row>
              <div className={`path position-absolute w-100 ${hide ? 'hide' : 'show'}`} 
              style={{bottom:0,left:0}}>
                <img src={path} className='img-fluid w-100' />
              </div>
            </Container>

            <div className={hide ? 'd-none' : 'd-block'}>
              <div id="top" style={{ backgroundColor: 'transparent' }}>
                  <Container fluid style={{backgroundColor:cmain}}>
                    <Container>
                    <Item>
                    <p className="fs24 text-center cblack py-4" style={{color:'white'}}>
                        <span className="fs16">
                        Dan di antara tanda-tanda (kekuasaan) Nya adalah Dia menciptakan untuk kamu pasangan-pasangan (hidup) dari jenis kamu sendiri, supaya kamu tenang kepadanya, dan dijadikan-Nya di antara kamu mawaddah dan rahmat. Sesungguhnya pada yang demikian itu benar-benar terdapat bukti-bukti bagi kaum yang berpikir (tentang kuasa dan nikmat Allah swt).
                             <br/><br/>( Q.S. Ar-Rum: 21 )
                        </span>
                    </p>
                  </Item>
                    </Container>
                  </Container>
                <Container className="dinny px-3 pt-5 ">
                  
                  <Item>
                      <Col xs={6} sm={2}>
                        <img src={bunga6} data-aos="zoom-in" data-aos-duration="1000" className='img-fluid w-100' />
                      </Col>
                    </Item>
                    <Item>
                            <p className='text-center p-2 px-4 ' style={{color:'black'}}>
                            Pernikahan Selly & Arief
                              </p>
                          </Item>
                </Container>
                <Container id='sectiongold55' className="py-5 dinny" >

                  <Item>
                    <div className=' col-xs-12 col-lg-6' data-aos="fade-left" data-aos-duration="1000">
                      <div className='kotak mr-lg-2'>
                        <Item>
                          <h1 style={{ fontSize: '72px', fontFamily: 'Alex Brush, cursive', color: 'cblack' }}>
                            Selly
                      </h1>
                        </Item>
                        <Item>
                          <Col xs={6}>
                            <img src={gambar('https://i.ibb.co/HHtddFZ/cewek.png', 90)} className='img-fluid w-100' />
                          </Col>
                        </Item>
                        <Item>
                          <h1 className="py-3" style={{ fontSize: '24px', fontFamily: "'Marck Script', cursive", 
                          color: 'black' }}>
                          Dede Selly Lestari
                      </h1>
                        </Item>
                        <Item>
                          <p className='text-center' style={{ fontSize: '20px', color: 'black' }}>
                            <b> Putri Kedua dari : </b><br />
                            Bapak Eeng Sunarya 
                        <br />
                        &<br />
                        Ibu Iis Suryani
                      </p>
                        </Item>
                        <Item>
                          <Col xs={4} sm={3} 
                            onClick={() => { window.open('https://instagram.com/selly_lestaryy') }}>
                            <img src={logoig} className='btn img-fluid w-100' />
                          </Col>
                        </Item>
                      </div>
                    </div>
                    <div className=' col-xs-12 mt-3 mt-lg-0  col-lg-6' data-aos="fade-right" data-aos-duration="1000">
                      <div className='kotak mr-lg-2'>
                        <Item>
                          <h1 style={{ fontSize: '72px', fontFamily: 'Alex Brush, cursive', color: black }}>
                            Arief
                      </h1>
                        </Item>
                        <Item>
                          <Col xs={6}>
                            <img src={gambar('https://i.ibb.co/847shwy/cowok.png', 90)} className='img-fluid w-100' />
                          </Col>
                        </Item>
                        <Item>
                          <h1 className="py-3" style={{ fontSize: '24px', fontFamily: "'Marck Script', cursive", 
                          color: 'black' }}>
                          Arief Reka Maulana, S.I.Kom
                      </h1>
                        </Item>
                        <Item>
                          <p className='text-center' style={{ fontSize: '20px', color: 'black' }}>
                            <b>Putra Ketiga dari : </b><br />
                            Bapak Khairussaleh 

                        <br />
                        &<br />
                        Ibu Ida Suparlina, S.Pd
                      </p>
                        </Item>
                        <Item>
                          <Col xs={4} sm={3} 
                            onClick={() => { window.open('https://instagram.com/arfreks') }}>
                            <img src={logoig} className='btn img-fluid w-100' />
                          </Col>
                        </Item>
                      </div>
                    </div>
                  </Item>
                </Container>
                <Container fluid className="text-center px-4 dinny" style={{ color: black }} >
                  <Item>
                    <p className="fs16">
                    Untuk melaksanakan syariat Agama-Mu dan menjalankan sunah Rasul-Mu dalam 
                    membina keluarga yang Sakinah, Mawadah, Warrahmah. InsyaAllah akan dilaksanakan 
                    Akad Nikah, <br/> pada :
                    </p>
                  </Item>
                  <Item>
                    <p className="fs20">
                      <b>
                        HARI JUMAT, <span className="fs36">{` 11 `}</span> DESEMBER 2020
                      </b>
                    </p>
                  </Item>
                  <Item>
                    <p className="fs20 cblack " >
                      <b>
                      PUKUL 08.00 s.d 11.30 WIB
                      </b>
                     <br />
                     <div style={{fontStyle:'italic',fontSize:'16px'}} className="pt-2">
                     Dikarenakan kondisi pandemi saat ini, sesuai dengan protokol kesehatan maka Akad Nikah akan dihadiri oleh keluarga dan kerabat terdekat saja.
                     </div>
                    </p>
                    
                    {/* <p className="px-3 d-none d-sm-block" style={{ color: mainfont, fontSize: '72px' }}>
                      \
                    </p>
                    <div className="col-8 d-sm-none" style={{ borderBottom: `2px solid ${cmain}` }}>
                    </div>
                    <p className="fs20 pt-3 pt-sm-0 col-sm-4" style={{ color: mainfont }}>
                      <b>Resepsi</b><br />
                      <span className="cblack fs16">
                        Sesi 1 : 11.00 WITA - 14.00 WITA 
                      </span> <br />
                      <span className="cblack fs16">
                      Sesi 2 : 19.00 WITA - selesai
                      </span>
                    </p> */}
                  </Item>
                  <Item>
                    <p className="fs16 pt-3 w-100 text-center">
                     Nanggerang RT 002 RW 014 Kel. Lembursitu, <br/>
                     
                        Kec. Lembursitu, Kota Sukabumi, 
                        <br/>Jawa Barat - 43168

                    </p>
                    

                  </Item>
                  <Item>
                    <Col xs={10} sm={3}
                      style={{
                        border: `2px solid ${cmain}`,
                        borderRadius: '10px'
                      }}
                      
                      onClick={() => {
                        window.open("https://goo.gl/maps/UpysfAwTdEeFy7dE6")
                      }}
                      className="p-2 mx-sm-2 btn">
                      <Item>
                        <img src="https://www.flaticon.com/svg/static/icons/svg/979/979874.svg" className="img-fluid"
                          style={{ width: "10%", height: '10%' }} />
                        <p className="mb-0 my-auto ml-3" style={{ color: 'black' }}>
                          <b>Get Direction</b>
                        </p>
                      </Item>
                    </Col>
                    <Col 
                      onClick={() => window.open('https://calendar.google.com/event?action=TEMPLATE&tmeid=N3FqcTBsYzZhNzlybmhjOWwwa2RhbjBmaDQgYXJpZWZjNzJAbQ&tmsrc=ariefc72%40gmail.com')}
                      xs={10} sm={3}
                      style={{
                        border: `2px solid ${cmain}`,
                        borderRadius: '10px'
                      }}
                      className="p-2 mx-sm-2 mt-3 mt-sm-0 btn">
                      <Item>
                        <img src="https://www.flaticon.com/svg/static/icons/svg/979/979863.svg" className="img-fluid"
                          style={{ width: "10%", height: "10%" }} />
                        <p className="mb-0 my-auto ml-3" style={{ color: 'black' }}>
                          <b>Add to Calendar</b>
                        </p>
                      </Item>
                    </Col>
                  </Item>
                </Container>
                <Container id='sectiongold57'>
                  <div className='py-3'>

                    <Item>
                      <div data-aos="fade-left" data-aos-duration="1000"
                        className='col-10 col-lg-8 kotak' style={{ backgroundColor: cmain }}>
                        <Item>
                          <div className='item'>
                            <Item>
                              <div>
                                {days}
                              </div>
                            </Item>
                            <Item>
                              <span>
                                Days
                                </span>
                            </Item>
                          </div>
                          <div className='dot'>:</div>
                          <div className='item'>
                            <Item>
                              <div>
                                {hours}
                              </div>
                            </Item>
                            <Item>
                              <span>
                                Hours
                      </span>
                            </Item>
                          </div>
                          <div className='dot'>:</div>
                          <div className='item'>
                            <Item>
                              <div >
                                {minutes}
                              </div>
                            </Item>
                            <Item>
                              <span>
                                Mins
                      </span>
                            </Item>
                          </div>
                          <div className='dot' >:</div>
                          <div className='item'>
                            <Item>
                              <div>
                                {seconds}
                              </div>
                            </Item>
                            <Item>
                              <span>
                                Secs
                      </span>
                            </Item>
                          </div>
                        </Item>

                      </div>
                    </Item>
                  </div></Container>
                <Container className="text-center dinny">
                  <Item>
                    <Col className="py-3 px-sm-5" style={{ border: `2px solid ${cmain}`, borderRadius: '10px' }}>
                      <h1 className="fs30"
                        style={{

                          fontFamily: "'Marck Script', cursive",
                          color: 'black'
                        }}>
                        Protokol Kesehatan
                          </h1>
                      <Item>
                        <Col xs={12}>
                          <p className="cblack text-center w-100 fs16">
                            Demi keamanan dan kenyamanan bersama, para hadirin undangan dihimbau untuk memenuhi beberapa peraturan berikut:
                          </p>
                        </Col>
                        <Col xs={6} sm={3}>
                          <img src={mask} className="w-100 img-fluid p-sm-4" />
                          <Row>
                            <p className="cblack text-center w-100 fs16">
                              Gunakan Masker
                            </p>
                          </Row>
                        </Col>
                        <Col xs={6} sm={3}>
                          <img src={distance} className="w-100 img-fluid p-sm-4" />
                          <Row>
                            <p className="cblack text-center w-100 fs16">
                              Jaga Jarak
                            </p>
                          </Row>
                        </Col>
                        <Col xs={6} sm={3}>
                          <img src={salaman} className="w-100 img-fluid p-sm-4" />
                          <Row>
                            <p className="cblack text-center w-100 fs16">
                              Cukup Bersalaman tanpa Bersentuhan
                          </p>
                          </Row>
                        </Col>
                        <Col xs={6} sm={3}>
                          <img src={cuci} className="w-100 img-fluid p-sm-4" />
                          <Row>
                            <p className="cblack text-center w-100 fs16">
                            Mencuci Tangan <br/> dengan Sabun 
                          </p>
                          </Row>
                        </Col>
                      </Item>
                    </Col>
                  </Item>

                  <Item>
                    <Col>
                    </Col>

                  </Item>

                </Container>

                <Container fluid style={{backgroundColor:cmain}}>

                <Container className='mt-3 py-3' data-aos="fade-right" data-aos-duration="1000">
                  <Slider slide={slider} />
                </Container>
                <Container id='sectiongold56'>
                  <div className='pt-3'>
                    
                    <div data-aos={`fade-right`} data-aos-duration="2000">
                      <Item>
                        <div className='kotak col-10' style={{ backgroundColor: 'white' }}>
                          <Item>
                            <p className='text-center p-1 px-4 mb-0' style={{color:'black',fontSize:'0.9rem'}}>
                            Whatever our souls are made of, his and mine are the same. <br/>- Emily Brontë -
                              </p>
                          </Item>
                        </div>

                      </Item>
                    </div>
                  </div>
                </Container>

                <Container id='sectiongold58' >

                  <div className='pt-3 mt-4 mt-lg-5 mb-lg-3'>
                    <Item>
                      <Col xs={4} lg={2}>
                        <img data-aos="zoom-in" data-aos-duration="1000" src={bunga6} className='img-fluid w-100' />
                      </Col>
                    </Item>
                    <Item>
                      <div className='col-10 col-lg-6 kotak pb-4 pt-4' data-aos="left-left" data-aos-duration="1000">
                        <Item>
                          <h1 style={{
                            fontFamily: '"Marck Script", cursive',
                            color:'black'
                          }}>
                            Kirim Ucapan
                        </h1>
                        </Item>
                        <Item>
                          <form className="col-12 w-100">
                            <input ref={this.nama} type='text' className="col-12 w-100 text-center" placeholder="Name" name='nama' defaultValue={query ? query : ''} />
                            <input ref={this.pesan} type='text-area' className="col-12 w-100 text-center bigger" placeholder="Message" name='pesan' />
                            <Item>
                              <div id="formradio">
                                <div class="custom_radio row justify-content-center">
                                  <div onClick={() => {
                                    this.setState({ hadir: true })
                                  }
                                  }>
                                    <input type="radio" id="featured-1" name="featured" checked={hadir ? true : false} />
                                    <label for="featured-1">Hadir</label>
                                  </div>
                                  <div onClick={() => {
                                    this.setState({ hadir: false })
                                  }
                                  } className="pl-5">
                                    <input type="radio" id="featured-2" name="featured" checked={hadir ? false : true} />
                                    <label for="featured-2"

                                    >Tidak Hadir</label>
                                  </div>
                                 
                                </div>
                              </div>
                            </Item>


                            <Item>
                              <Col xs={12} className=''>
                                {
                                  submitted == true ? (
                                    <Alert variant='success' style={{fontSize:'16px'}}>
                                      Pesan anda sudah disampaikan
                                    </Alert>) : (submitted === false ? (
                                      <Alert variant='danger' style={{fontSize:'16px'}}>
                                        {
                                          err.map(val => {
                                            return (
                                              <li>{val}</li>
                                            )
                                          })
                                        }

                                      </Alert>
                                    ) : false)
                                }

                              </Col>
                            </Item>
                            <Item>
                              <div className='col-6 button rounded btn'
                                onClick={() => this.handleSubmit()} style={{ backgroundColor: cmain }} no> Kirim </div>
                            </Item>
                          </form>
                        </Item>
                      </div>
                    </Item>
                  </div>
                </Container>
                <Container className="py-3">
                  <h1 style={{color:'white'}} className="alex w-100 text-center">
                    Selly 
                               {' & '}
                    Arief 
                  </h1>
                  
                </Container>
                <Container className='text-center pb-5' id='sectiongold59'>
                  <Item>
                    <h1 data-aos="zoom-in" data-aos-duration="1000" style={{color:'white'}}>
                      Possible Wedding
              </h1>
                  </Item>
                  <Item>
                    <h2 data-aos="zoom-in" data-aos-duration="1000" style={{color:'white'}}>
                      Digital Invitation
              </h2>
                  </Item>
                  <Item>
                    <div className='col-4 col-lg-1' onClick={()=>{window.open('https://instagram.com/possiblewedding')}} >
                      <img src={logoig} className='img-fluid w-100 btn'
                      />
                    </div>
                  </Item>

                </Container>
                </Container>
              
              </div>
            </div>
          </div>
        </div>

      </>


    )
  }
}

