import React, { Component } from 'react' // Import
import Container from 'react-bootstrap/Container'
import Header, { Item, Float, Foot, Slider } from '../components/main'
import { Helm } from '../components/header'
import { Form, Row, Col, Alert,Button } from 'react-bootstrap'
import { cap } from '../params'
import '../style/style.css'
import '../style/sty.scss'
import logoig from '../assets/img/nasta/logoig.svg'
import burung from '../assets/img/nasta/burung.svg'
import bunga6 from '../assets/img/bunga6.png'
import AOS from 'aos';
import { gambar, pw } from '../params'
import { Timer } from '../components/timer'
import post from '../params/post'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import logo from '../assets/img/logo.ico'
import covid from '../assets/img/nasta/covid.png'
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Gift } from '../components/gift'

let cmain = '#A9865B'
let black = 'rgb(38,38,38)'

let id = 'feby-tunggul'
let inisial_co = 'Tunggul'
let inisial_ce = 'Feby'

let lengkap_co = (<>Warih Tunggul Wulung</>)
let lengkap_ce = (<>Prasma Feby Rahmadhani</>)

let bapak_co = 'Alm. Bpk Hariadi Soesito'
let ibu_co = 'Ibu Sri Hartati'
let bapak_ce = "Bpk Pardi"
let ibu_ce = "Ibu Amiek Sri Supadmi"

let ig_co = "warihtunggul"
let ig_ce = "prasmafeby"

let foto_ce = pw(id, "ce.jpg")
let foto_co = pw(id, "co.jpg")
let waktunikah = "01/22/2021"

let modal = pw(id, "modal.jpg")
let openlogo = pw(id, "logo.png")

let gmaps = "https://goo.gl/maps/j9zbYnDj2dtjiKMm8"
let gcalendar = 'https://calendar.google.com/event?action=TEMPLATE&tmeid=NGw1M3JiZmFpaXRjZGhnb2RxMjcwaGJlaGMgYXJpZWZjNzJAbQ&tmsrc=ariefc72%40gmail.com'
let gmaps1 = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.758772819372!2d106.73329051476942!3d-6.295399295443682!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f01b2d848e5d%3A0xedab0a0291d19954!2sBuni%20Manten!5e0!3m2!1sid!2sid!4v1608862743657!5m2!1sid!2sid"



export default class Halo extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef()
        this.myRef = React.createRef()
        this.nama = React.createRef()
        this.alamat = React.createRef()
        this.pesan = React.createRef()

        this.state = {
            acara: [],
            days: '00',
            minutes: '00',
            hours: '00',
            seconds: '00',
            hide: true,
            hadir: true,
            err: [],
            submitted: '',
            sesi: 0,
            showgift: false
        }
    }

    componentDidMount() {
        AOS.init({
            // initialise with other settings
            duration: 2000
        });

    }

    play = () => {
        AOS.refresh()
        var snd = new Audio(pw(id, "music.mp3"));
        snd.type = 'audio/mp3';
        snd.play();
        this.setState({ hide: false })
        setTimeout(() => {
            var elmnt = document.getElementById('top');
            elmnt.scrollIntoView();
        }, 1000)
    }

    useQuery = () => {
        return new URLSearchParams(this.props.location.search);
    }
    handleSubmit = async () => {
        let err = []
        let local = localStorage.getItem('pesan')
        if (this.nama.current.value == "") {
            err.push('Nama tidak Boleh Kosong')
        }
        if (this.pesan.current.value == "") {
            err.push('Pesan tidak Boleh Kosong')
        }
        if (err.length == 0) {
            confirmAlert({
                message: local ? `Kami mendeteksi bahwa anda telah mengirimkan pesan \" ${local} \", apakah anda ingin mengirim pesan lagi?` : 'Yakin untuk Mengirim Pesan?',
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async () => {
                            let send = await post(
                                ` dari: "${encodeURI(this.nama.current.value)}", hadir: "", jumlahhadir: "", kepada: "${id}", pesan:"${encodeURI(this.pesan.current.value)}",alamat: ""`
                            )
                            if (send.status == 200) {
                                this.setState({ submitted: true })
                                localStorage.setItem('pesan', this.pesan.current.value)
                                this.nama.current.value = ''
                                this.pesan.current.value = ''
                            } else {
                                err.push('Koneksi Gagal')
                            }


                        }
                    },
                    {
                        label: 'No',
                        onClick: () => { }
                    }
                ]
            });
        } else {
            this.setState({ err: err, submitted: false })
        }
    }
    render() {
        let { hadir, showgift, hide, submitted, err, sesi } = this.state
        let slider = []
        let slide = [
            "RJ-Ameng-00132.jpg",
            "RJ-Ameng-00156.jpg",
            "RJ-Ameng-00306.jpg",
            "RJ-Ameng-00549.jpg",
            "RJ-Ameng-00631.jpg",
            "RJ-Ameng-00796.jpg",
            "RJ-Ameng-00845.jpg",
            "RJ-Ameng-00866.jpg",
            "RJ-Ameng-00929 - Copy.jpg",
            "RJ-Ameng-00987.jpg",
        ]
        slide.map(v => {
            slider.push(gambar(pw(id, v), 95, 'auto&func=fit&bg_img_fit=1&bg_opacity=0.75&w=1440&h=720'))
        })
        let query = this.useQuery().get('u');
        query = query ? cap(query) : ''

        return (
            <>
                <Helm
                    title={`Undanganku - ${inisial_ce} & ${inisial_co}`}
                    desc="undangan digital berbasis website untuk berbagai kebutuhan acara"
                    logo={logo}
                    img={slider[0]}
                    url={`https://undang.in/${id}`}
                >
                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                    <link href="https://fonts.googleapis.com/css2?family=Numans&display=swap" rel="stylesheet"></link>
                    <link href="https://fonts.googleapis.com/css2?family=Merienda:wght@400;700&display=swap" rel="stylesheet" />
                    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow:wght@400;700&display=swap" rel="stylesheet" />
                    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet"></link>
                </Helm>

                <div id='gold5' style={{
                    backgroundColor: '#F7EEE6'
                }}>
                    {
                        this.useQuery().get('x') == "x" ? (<Float />) : false
                    }
                    <div className='w-100' style={{
                        overflow: 'hidden', maxWidth: '100vw',
                        backgroundColor: 'transparent'
                    }}>
                        <Container fluid id='g3-header' className='relative' style={{
                            backgroundImage: `url('${modal}')`
                        }}>
                            <Item>
                                <Col xs={12} md={4} className='m-md-0 '>
                                    <img className='img-fluid w-100 px-4 py-2'
                                        src={gambar(openlogo)} data-aos="fade-left" />
                                </Col>
                            </Item>
                            
                            <Item>
                                {
                                    <h2 className={`col-md-4 roboto-slab text-center pt-3 pt-sm-3`} style={{ marginTop: '0' }}>
                                        Kepada Yth :<br /> {query ? query : ''} <br /></h2>
                                }</Item>
                            <Row className='justify-content-center'>
                                <div onClick={() => { this.play() }}

                                    className={`col-md-4 button btn roboto-slab text-center ${hide ? 'show' : 'hide'}`}
                                    style={{ marginTop: 0, color: 'white' }}>
                                    Open Invitation
                            </div>
                            </Row>
                        </Container>

                        <div className={hide ? 'd-none' : 'd-block'}>
                            <div id="top" style={{ backgroundColor: 'transparent' }}>
                                <Container className="dinny px-3 pt-5 ">
                                    <Item>

                                        <p className="fs16 text-center cblack px-3"
                                            style={{
                                                fontFamily: 'Numans, sans-serif',
                                                fontSize: '16px'
                                            }}
                                        >
                                            “Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu isteri-isteri dari jenismu sendiri, supaya kamu cenderung dan merasa tenteram kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berfikir.” <br /><br />(Ar-Rum: 21)

                                        </p>
                                    </Item>

                                    <Item>
                                        <Col xs={6} sm={2}>
                                            <img src={pw(id, "leaf.svg")} data-aos="zoom-in" data-aos-duration="1000" className='img-fluid w-100' />
                                        </Col>
                                    </Item>

                                    <Item>
                                        <p className='text-center p-2 px-4 ' style={{
                                            color: cmain,
                                            fontFamily: 'Playfair Display, serif'
                                        }}>
                                            The Intimate Wedding of <br /> {inisial_ce} & {inisial_co}
                                        </p>

                                    </Item>
                                </Container>
                                <Container id='sectiongold55' className="py-5 dinny" >

                                    <Item>
                                        <div className=' col-xs-12 col-lg-6' data-aos="fade-left" data-aos-duration="1000">
                                            <div className=' mr-lg-2'>

                                                <Item>
                                                    <Col xs={6}>
                                                        <img src={gambar(foto_ce, 90)} className='img-fluid w-100 rounded-circle'
                                                            style={{ border: `4px solid ${cmain}` }} />
                                                    </Col>
                                                </Item>
                                                <Item>
                                                    <h1 className="py-3 w-100 text-center"
                                                        style={{ fontSize: '32px', fontFamily: 'Playfair Display, serif', color: cmain }}>
                                                        {lengkap_ce}
                                                    </h1>
                                                </Item>
                                                <Item>
                                                    <p className='text-center' style={{
                                                        fontFamily: 'Playfair Display, serif',
                                                        fontSize: '16px', color: 'black'
                                                    }} >
                                                        <b style={{ fontFamily: 'Numans, sans-serif' }}>Putri dari :</b><br />
                                                        {bapak_ce}  <br />
                                                        &<br />
                                                        {ibu_ce}
                                                    </p>
                                                </Item>
                                                <Item>

                                                    <img src={pw(id, "logoig.svg")} className='btn p-0'
                                                        onClick={() => { window.open(`https://instagram.com/${ig_ce}`) }} width="35px" height="35px" />

                                                </Item>
                                            </div>
                                        </div>
                                        <div className=' col-xs-12 mt-3 mt-lg-0  col-lg-6' data-aos="fade-right" data-aos-duration="1000">
                                            <div className=' mr-lg-2'>
                                                <Item>
                                                    <Col xs={6}>
                                                        <img src={gambar(foto_co, 90)} className='img-fluid w-100 rounded-circle'
                                                            style={{ border: `4px solid ${cmain}` }} />
                                                    </Col>
                                                </Item>
                                                <Item>
                                                    <h1 className="py-3 w-100 text-center" style={{
                                                        fontSize: '32px',
                                                        fontFamily: 'Playfair Display, serif', color: cmain
                                                    }}>
                                                        {lengkap_co}
                                                    </h1>
                                                </Item>
                                                <Item>
                                                    <p className='text-center' style={{
                                                        fontFamily: 'Playfair Display, serif',
                                                        fontSize: '16px', color: 'black'
                                                    }} >
                                                        <b style={{ fontFamily: 'Numans, sans-serif' }}>Putra dari:</b><br />
                                                        {bapak_co}
                                                        <br />
                                                        &<br />
                                                        {ibu_co}
                                                    </p>
                                                </Item>
                                                <Item>
                                                    <img src={pw(id, "logoig.svg")} className='btn p-0'
                                                        onClick={() => { window.open(`https://instagram.com/${ig_co}`) }} width="35px" height="35px" />

                                                </Item>
                                            </div>
                                        </div>
                                    </Item>
                                    <p className="fs16 text-center cblack p-3 w-100"
                                            style={{
                                                fontFamily: 'Numans, sans-serif',
                                                fontSize: '16px'
                                            }}
                                        >
                                             Mengingat situasi pandemi covid-19 yang belum mereda, kami mohon maaf <br />tidak dapat mengundang Bapak/Ibu/Saudara/i untuk hadir pada acara kami secara langsung.
                                
                                        </p>
                                        <Item>
                                        <Button style={{backgroundColor:cmain,color:'white'}} 
                                        onClick={()=>{
                                            window.open(`https://instagram.com/${ig_ce}`)
                                        }}>
                                            Live Instagram 
                                        </Button>
                                        </Item>
                                </Container>
                                <Container fluid style={{ backgroundColor: '#634837' }} className="p-3 p-md-5">
                                    <Container>
                                        <img src={pw(id,"RJ-Ameng-00845.jpg")} className="img-fluid w-100"/>
                                    </Container>
                                </Container>
                                <Container className="py-3" style={{ fontFamily: 'Numans, Sans-serif', color: 'black', fontSize: '16px' }}>
                                    <Item>
                                        <Col xs={6} sm={2}>
                                            <img src={pw(id, "leaf.svg")} data-aos="zoom-in" data-aos-duration="1000" className='img-fluid w-100' />
                                        </Col>
                                    </Item>
                                    <Item>
                                        <p className="text-center py-3">
                                        Assalamu’alaikum Warahmatullahi Wabarakatuh<br/>
Dengan memohon rahmat dan ridho Allah SWT, kami bermaksud untuk menyelenggarakan pernikahan<br/>
putra-putri kami, yang InsyaAllah akan dilaksanakan pada :<br/>
                                            </p>
                                    </Item>

                                    <Item>
                                        <p className="text-center py-3" style={{ color: cmain }}>
                                            <b>JUMAT<br /><br />
                                                    22 / JANUARI / 2021
                                                    </b><br />
                                        </p>

                                    </Item>
                                    <Item>
                                        <Col xs={6} sm={2}>
                                            <img src={pw(id, "leaf.svg")} data-aos="zoom-in" data-aos-duration="1000" className='img-fluid w-100' />
                                        </Col>
                                    </Item>
                                    <Item>
                                        <p className="text-center py-3" style={{ color: cmain }}>
                                            <b>WAKTU<br /><br />
                                                    15.00 WIB - SELESAI
                                                    </b><br />
                                        </p>

                                    </Item>
                                    <Item>
                                        <Col xs={6} sm={2}>
                                            <img src={pw(id, "leaf.svg")} data-aos="zoom-in" data-aos-duration="1000" className='img-fluid w-100' />
                                        </Col>
                                    </Item>
                                    <Item>
                                        <p className="text-center py-3" style={{ color: cmain }}>
                                            <b>TEMPAT<br /><br />
                                            <b>Buni Manten</b>
                                            <br />
                                            Ciputat, Tangerang Selatan
                                                    </b><br />
                                            (Hanya Untuk Keluarga)
                                        </p>

                                    </Item>
                                    
                                    <Item>
                                        <Col xs={6} sm={2}>
                                            <img src={pw(id, "leaf.svg")} data-aos="zoom-in" data-aos-duration="1000" className='img-fluid w-100' />
                                        </Col>
                                    </Item>
                                    <Item>
                                        <p className="text-center pt-3" style={{ color: cmain }}>
                                            <b> 
                                                "It is a truth universally acknowledged, that a single man
                                                 in possession of a good fortune, must be in want of a wife"
                                                 </b>
                                        </p>
                                        

                                    </Item>
                             
                                    <Timer cmain={cmain} waktunikah={waktunikah} />
                                    <Item>
                                        
                                        <Col
                                            onClick={() => window.open(gcalendar)}
                                            xs={10} sm={3}
                                            style={{
                                                border: `2px solid ${cmain}`,
                                                borderRadius: '10px'
                                            }}
                                            className="p-2 mx-sm-2 mt-3 mt-sm-0 btn">
                                            <Item>
                                                <img src="https://www.flaticon.com/svg/static/icons/svg/979/979863.svg" className="img-fluid"
                                                    style={{ width: "10%", height: '10%' }} />
                                                <p className="mb-0 my-auto ml-3" style={{ color: cmain }}>
                                                    <b>Add to Calendar</b>
                                                </p>
                                            </Item>
                                        </Col>
                                    </Item>

                                </Container>
                                <Container fluid style={{ backgroundColor: '#634837' }} className="p-3 p-md-5">
                                    <Container>
                                        
                                            <p className="w-100 text-center"
                                            style={{fontFamily:"Numans, Sans-serif",fontSize:'16px',color:'white'}}>
                                            Gallery Photos
                                            </p>
                                            <p className="w-100 text-center"
                                            style={{fontFamily:"Playfair Display, Sans-serif",fontSize:'48px',color:cmain}}>
                                            {inisial_ce} & {inisial_co}
                                            </p>
                                       
                                    <Slider slide={slider} />
                                    <Item>
                                        <Col xs={6} sm={2} className="py-5">
                                            <img src={pw(id, "leafwhite.svg")} data-aos="zoom-in" data-aos-duration="1000" className='img-fluid w-100' />
                                        </Col>
                                    </Item>
                                    <p className="w-100 text-center"
                                            style={{fontFamily:"Numans, Sans-serif",fontSize:'16px',color:'white'}}>
                                           {inisial_ce} & {inisial_co}
                                            </p>
                                            <p className="w-100 text-center"
                                            style={{fontFamily:"Playfair Display, Sans-serif",fontSize:'48px',color:cmain}}>
                                            VENUE
                                            </p>
                                            <Item>
                                        <div className="mapouter m-3"><div className="gmap_canvas text-center">
                                            <iframe width="400" height="300" id="gmap_canvas"
                                                src={gmaps1} frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                                            </iframe></div>
                                        </div>
                                    </Item>
                                    <Item>
                                    <Col xs={10} sm={3}
                                            style={{
                                                border: `2px solid white`,
                                                borderRadius: '10px'
                                            }}
                                            onClick={() => {
                                                window.open(gmaps)
                                            }}
                                            className="p-2 mx-sm-2 btn">
                                            <Item>
                                                <img src="https://www.flaticon.com/svg/static/icons/svg/979/979874.svg" className="img-fluid"
                                                    style={{ width: "10%", height: '10%' }} />
                                                <p className="mb-0 my-auto ml-3" style={{ color: 'white' }}>
                                                    <b>Get Direction</b>
                                                </p>
                                            </Item>
                                        </Col>
                                    </Item>
                                    </Container>
                                </Container>
                                                              

                                <Container className="py-3">
                                    <Item>
                                        <Col xs={12} md={6}>
                                            <img src={pw("asset","covid-brown.svg")} className="w-100 img-fluid" />
                                        </Col>
                                    </Item>
                                </Container>


                                
                                <Container id='sectiongold56'>
                                    <div className='pt-3'>

                                        <div data-aos={`fade-right`} data-aos-duration="2000">
                                            <Item>
                                                <div className='kotak col-10' style={{ backgroundColor: cmain }}>
                                                    <Item>
                                                        <p className='text-center p-2 px-4 fs14'>
                                                            “Love recognizes no barriers. It jumps hurdles, leaps fences, penetrates walls to arrive at its destination full of hope.”
<br />– Maya Angelou

</p>
                                                    </Item>
                                                </div>

                                            </Item>
                                        </div>
                                    </div>
                                </Container>

                                <Container id='sectiongold58' >

                                    <div className='pt-3 mt-4 mt-lg-5 mb-lg-3'>
                                        <Item>
                                            <Col xs={4} lg={2}>
                                                <img data-aos="zoom-in" data-aos-duration="1000" src={bunga6} className='img-fluid w-100' />
                                            </Col>
                                        </Item>
                                        <Item>
                                            <div className='col-10 col-lg-6 kotak pb-4 pt-4' data-aos="left-left" data-aos-duration="1000">
                                                <Item>
                                                    <h1 className="w-100 text-center" style={{
                                                        fontFamily:"Playfair Display, Sans-serif",
                                                        color: cmain
                                                    }}>
                                                        Send Your Wishes
                    </h1>
                                                </Item>
                                                <Item>
                                                    <form className="col-12 w-100">
                                                        <input ref={this.nama} type='text' className="col-12 w-100 text-center" placeholder="Name" name='nama' />
                                                        <input ref={this.pesan} type='text-area' className="col-12 w-100 text-center bigger" placeholder="Message" name='pesan' />
                                                        {
                                                            sesi == 1 || sesi == 2 ? (
                                                                <Item>
                                                                    <div id="formradio">
                                                                        <div class="custom_radio row justify-content-center">
                                                                            <div onClick={() => {
                                                                                this.setState({ hadir: true })
                                                                            }
                                                                            }>
                                                                                <input type="radio" id="featured-1" name="featured" checked={hadir ? true : false} />
                                                                                <label for="featured-1">Hadir</label>
                                                                            </div>
                                                                            <div onClick={() => {
                                                                                this.setState({ hadir: false })
                                                                            }
                                                                            } className="pl-5">
                                                                                <input type="radio" id="featured-2" name="featured" checked={hadir ? false : true} />
                                                                                <label for="featured-2"

                                                                                >Tidak Hadir</label>
                                                                            </div>
                                                                            {!hadir ? false : (
                                                                                <>  <Alert variant='dark col-12 mr-4 '>

                                                                                    {
                                                                                        sesi == 1 ? (<div onClick={() => {
                                                                                            this.setState({ sesi: 1 })
                                                                                        }
                                                                                        }
                                                                                            className="pl-5">
                                                                                            <input type="radio" id="featured-3" name="sesi" checked={sesi == 1 ? true : false} />
                                                                                            <label for="featured-3">

                                                                                                18.30 - 19.30 WIB
                                                                                    </label>
                                                                                        </div>) : (
                                                                                                <div onClick={() => {
                                                                                                    this.setState({ sesi: 2 })
                                                                                                }
                                                                                                } className="pl-5">
                                                                                                    <input type="radio" id="featured-4" name="sesi" checked={sesi == 2 ? true : false} />
                                                                                                    <label for="featured-4"

                                                                                                    >19.30 - 20.30 WIB</label>
                                                                                                </div>
                                                                                            )
                                                                                    }


                                                                                </Alert>



                                                                                </>

                                                                            )}

                                                                        </div>
                                                                    </div>
                                                                </Item>


                                                            ) : false
                                                        }
                                                        <Item>
                                                            <Col xs={12} className=''>
                                                                {
                                                                    submitted == true ? (
                                                                        <Alert variant='success' style={{ fontSize: '16px' }}>
                                                                            Pesan anda sudah disampaikan
                                                                        </Alert>) : (submitted === false ? (
                                                                            <Alert variant='danger' style={{ fontSize: '16px' }}>
                                                                                {
                                                                                    err.map(val => {
                                                                                        return (
                                                                                            <li>{val}</li>
                                                                                        )
                                                                                    })
                                                                                }

                                                                            </Alert>
                                                                        ) : false)
                                                                }

                                                            </Col>
                                                        </Item>
                                                        <Item>
                                                            <div className='col-6 button rounded btn'
                                                                onClick={() => this.handleSubmit()} style={{ backgroundColor: cmain, color: 'white' }} no> Kirim </div>
                                                        </Item>
                                                    </form>
                                                </Item>
                                            </div>
                                        </Item>
                                    </div>
                                </Container>
                                <Container className="py-3" fluid >
                                    <Item>
                                        <Col
                                            onClick={() => this.setState({ showgift: !showgift })}
                                            xs={10} md={4}
                                            style={{
                                                border: `2px solid ${cmain}`,
                                                borderRadius: '10px'
                                            }}
                                            className="p-2 mx-md-2 mt-3 mt-md-0">
                                            <Item>
                                                <img src="https://www.flaticon.com/svg/static/icons/svg/1139/1139982.svg" className="img-fluid"
                                                    style={{ width: "10%", height: '10%' }} />
                                                <p className="mb-0 my-auto ml-3" style={{ color: cmain }}>
                                                    <b>Send Gift (click here)</b>
                                                </p>
                                            </Item>
                                        </Col>
                                    </Item>
                                </Container>
                                {showgift ? (<Gift
                                    reza
                                    rinda
                                    prep
                                    content={
                                        [
                                          
                                            {
                                                bank: 'BCA',
                                                norek: '6040918840',
                                                nama: 'Prasma Feby Rahmadhani'
                                            },
                                            {
                                                bank: 'Bukopin',
                                                norek: '4202012362',
                                                nama: 'Prasma Feby Rahmadhani'
                                            },
                                            {
                                                bank: 'BCA',
                                                norek: '6042209887',
                                                nama: 'Warih Tunggul Wulung'
                                            },
                                            {
                                                bank: 'Bukopin',
                                                norek: '4202013212',
                                                nama: 'Warih Tunggul Wulung'
                                            },

                                        ]
                                    }
                                    caption='For those who want to give gifts to our wedding, kindly transfer to the following accounts :'
                                    ccaption="black"
                                    color={'black'}
                                    bg={cmain}
                                    langsung
                                    alamat={<>Cinere Insani Residence<br/>
                                        blok E 2.RT 06 , RW 04<br/>
                                        jln meruyung raya, rangkapan jaya baru, pancoran mas .depok<br/>
                                        16434</>}
                                />) : false}

                                <Foot ig={pw(id, "logoig.svg")} dark />
                            </div>
                        </div>
                    </div>
                </div>

            </>


        )
    }
}

