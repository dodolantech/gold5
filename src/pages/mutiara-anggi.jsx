import React, { Component } from 'react' // Import
import Container from 'react-bootstrap/Container'
import  { Item, Float, Foot, Slider } from '../components/main'
import { Helm } from '../components/header'
import { Row, Col, Alert,Dropdown,DropdownButton, } from 'react-bootstrap'
import { cap,pw } from '../params'
import '../style/style.css'
import '../style/sty.scss'
import music from '../assets/music/dea.mp3'
import logoig from '../assets/img/dinny/logoig.svg'
import bismillah from '../assets/img/dea/bismillah.svg'
import bunga6 from '../assets/img/bunga6.png'
import AOS from 'aos';
import { gambar } from '../params'
import post from '../params/post'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import logo from '../assets/img/logo.ico'
import covid from '../assets/img/fitri/covid.svg'
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';

let cmain = '#636E72'
let orange = '#B99225'
let black = 'rgb(38,38,38)'
let id='mutiara-anggi'
let id_temp = 'dea-dayat'
let inisial_co = 'Anggi'
let inisial_ce = 'Mutiara'

let lengkap_co = (<>Anggi Setia Nugraha</>)
let lengkap_ce = (<>Mutiara Putri Reymona</>)

let bapak_co = 'Bpk Aping Setiadi '
let ibu_co = 'Ibu Ai Mulyati'
let bapak_ce = "Bpk Munazir"
let ibu_ce = "Ibu Ristyawani"

let ig_co = "Anggisn10"
let ig_ce = "Mutiarey"

let foto_ce = "https://i.ibb.co/WHrgx9M/Individu-2.jpg"
let foto_co = "https://i.ibb.co/wRYZWc4/Individu-1.jpg"
let waktunikah = "03/06/2021"

let modal = gambar(pw(id,"EAS_0845 edit.jpg"), 95, 'auto&func=fit&bg_img_fit=1&bg_opacity=0.75&w=1440&h=720')
let openlogo = pw(id,"logo.png")

let gmaps = "https://goo.gl/maps/WU6p4MLjGC12xxue7"
let gcalendar = 'https://calendar.google.com/event?action=TEMPLATE&tmeid=NTNsdGpkbmxhbmVicG04YjFhcTAzZTVsc24gYXJpZWZjNzJAbQ&tmsrc=ariefc72%40gmail.com'

let slide = ["EAS_0826.jpg",
"EAS_0837 edit.jpg",
"EAS_0845 edit.jpg",
"EAS_0853 edit.jpg",
"EAS_0856.jpg",
"EAS_0888 edit.jpg",
"EAS_0892.jpg",
"EAS_0900.jpg",
"EAS_876 edit.jpg",
]

export default class Halo extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef()
        this.myRef = React.createRef()
        this.nama = React.createRef()
        this.alamat = React.createRef()
        this.pesan = React.createRef()

        this.state = {
            acara: [],
            days: '00',
            minutes: '00',
            hours: '00',
            seconds: '00',
            hide: true,
            hadir: true,
            err: [],
            submitted: '',
            sesi: 2,
            friend:0
        }
    }

    componentDidMount() {
        AOS.init({
            // initialise with other settings
            duration: 2000
        });
        var countDownDate = new Date(waktunikah).getTime();

        // Update the count down every 1 second
        var x = setInterval(() => {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            this.setState({
                days: days, hours: hours, minutes: minutes, seconds: seconds
            })

        }, 1000);

    }

    play = () => {
        AOS.refresh()
        // var snd = new Audio(music);
        // snd.type = 'audio/mp3';
        // snd.play();
        this.setState({ hide: false })
        setTimeout(() => {
            var elmnt = document.getElementById('top');
            elmnt.scrollIntoView();
        }, 1000)
    }

    useQuery = () => {
        return new URLSearchParams(this.props.location.search);
    }
    handleSubmit = async () => {
        let err = []
        let local = localStorage.getItem('pesan')
        if (this.nama.current.value == "") {
            err.push('Nama tidak Boleh Kosong')
        }
        if (this.pesan.current.value == "") {
            err.push('Pesan tidak Boleh Kosong')
        }
        if (err.length == 0) {
            confirmAlert({
                message: local ? `Kami mendeteksi bahwa anda telah mengirimkan pesan \" ${local} \", apakah anda ingin mengirim pesan lagi?` : 'Yakin untuk Mengirim Pesan?',
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async () => {
                            let send = await post(
                                ` dari: "${encodeURI(this.nama.current.value)}", hadir: "${this.state.hadir}", jumlahhadir: "${this.state.friend}", kepada: "${id}", pesan:"${encodeURI(this.pesan.current.value)}",alamat: ""`
                            )
                            if (send.status == 200) {
                                this.setState({ submitted: true })
                                localStorage.setItem('pesan', this.pesan.current.value)
                                this.nama.current.value = ''
                                this.pesan.current.value = ''
                            }else{
                                err.push('Koneksi Gagal')
                            }
                        }
                    },
                    {
                        label: 'No',
                        onClick: () => { }
                    }
                ]
            });
        } else {
            this.setState({ err: err, submitted: false })
        }


    }
    dropdown = () => {
        let { friend } = this.state
        let item = []
        for (let index = 1; index < 3; index++) {
            item[index] = (<Dropdown.Item onClick={() => {
                this.setState({ friend: index })
            }}>{index}</Dropdown.Item>)
        }
        return (
            <>
                <Item>
                    <p style={{ fontSize: '16px', color: cmain }} className="mb-0 w-100 text-center">
                        How many people you coming with?
          </p>
                    <DropdownButton id="dropdown-basic-button" title={friend == null ? '1' : friend}>
                        {
                            item
                        }
                    </DropdownButton>
                </Item>
            </>
        )
    }
    render() {
        let { hadir, days, hours, minutes, seconds, hide, submitted, err, sesi } = this.state
        let slider = []
        slide.map(v => {
            slider.push(gambar(pw(id,v), 95, 'auto&func=fit&bg_img_fit=1&bg_opacity=0.75&w=1440&h=720'))
        })
        let query = this.useQuery().get('name');
        query = query ? cap(query) : ''

        return (
            <>
                <Helm
                    title={`Undanganku - ${inisial_ce} & ${inisial_co}`}
                    desc="undangan digital berbasis website untuk berbagai kebutuhan acara"
                    logo={logo}
                    img={slide[0]}
                    url={`https://undang.in/${id}`}
                />

                <div id='gold5' style={{
                    backgroundColor: '#636E72'
                }}>
                    {
                        this.useQuery().get('x') == "x" ? (<Float />) : false
                    }
                    <div className='w-100' style={{
                        overflow: 'hidden', maxWidth: '100vw',
                        backgroundColor: 'transparent'
                    }}>
                        <Container fluid id='g3-header' className='position-relative' style={{
                            backgroundImage: `url('${modal}')`
                        }}>
                            <div className="position-absolute" style={{ left: 0, bottom: '10%', width: '100vw' }}>
                                <Item>
                                    <Col xs={12} md={4} className='m-2 m-md-0 '>
                                        <img className='img-fluid w-100 p-2'
                                            src={openlogo} data-aos="fade-left" data-aos="fade-left" />
                                    </Col>
                                </Item>
                                <Item>
                                    {
                                        <h2 className={`col-md-4 roboto-slab text-center pt-3 pt-sm-3`}
                                            style={{ marginTop: '0' }} data-aos="fade-right">
                                            Kepada Yth :<br /> {query ? query : ''} <br /></h2>
                                    }</Item>
                                <Row className='justify-content-center'>
                                    <div onClick={() => { this.play() }}
                                        // data-aos="fade-left"
                                        className={`col-md-4 button btn roboto-slab text-center ${hide ? 'show' : 'hide'}`}
                                        style={{ marginTop: 0, color: 'white' }}>
                                        Open Invitation
                            </div>
                                </Row>

                            </div>
                        </Container>

                        <div className={hide ? 'd-none' : 'd-block'}>
                            <div id="top" style={{ backgroundColor: 'transparent' }}>
                                <Container className="dinny px-3 pt-5 ">
                                <Item>
                                    <Col xs={12} md={8}>
                                        <p className='italic roboto-slab c-main text-center'
                                        style={{color:'white',fontSize:'18px'}}
                                        data-aos="fade-right">
                                            بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم
                                    <br /><br />
                                    وَمِنْ ءَايَٰتِهِۦٓ أَنْ خَلَقَ لَكُم مِّنْ أَنفُسِكُمْ أَزْوَٰجًا لِّتَسْكُنُوٓا۟ إِلَيْهَا وَجَعَلَ بَيْنَكُم مَّوَدَّةً وَرَحْمَةً ۚ إِنَّ فِى ذَٰلِكَ لَءَايَٰتٍ لِّقَوْمٍ يَتَفَكَّرُونَ
 <br />
                                        </p>
                                        <p className='italic roboto-slab c-main f-hu text-center' data-aos="fade-right"
                                        style={{color:'white',fontSize:'16px'}}>
                                            “Dan di antara tanda-tanda kekuasaan-Nya lah Dia menciptakan untukmu istri-istri dari jenismu sendiri, supaya kamu cenderung dan merasa tentram kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berfikir.” <br /><br />(Ar-Rum: 21)
                            </p>
                                    </Col>
                                </Item>
                                    <Item>
                                        {/* <Col xs={10} md={4}>
                                            <img src={bismillah} className="img-fluid w-100" />
                                        </Col> */}
                                        <p className="fs16 text-center  px-3 py-3 w-100 text-center"
                                            style={{ color: 'white',fontSize:'16px' }} data-aos="fade-left">
                                            Assalaamu'alaikum Warahmatullahi  Wabarakatuh<br /><br />
                                            Dengan memohon rahmat dan ridha Allah SWT,<br />
                                            kami bermaksud menyelenggarakan pernikahan putra-putri kami
                                        </p>
                                    </Item>
                                    
                                </Container>
                                
                                <Container style={{ color: 'white' }} className="py-5">
                                    <Item>
                                        <Col md={5}>
                                            <h1 className="w-100 text-center"
                                                style={{
                                                    fontFamily: 'Parisienne, cursive', color: orange
                                                }} data-aos="fade-left">
                                                {lengkap_ce}
                                            </h1>
                                            <Item>
                                                <img src={logoig} className='btn p-0 my-3'
                                                    data-aos="fade-right"
                                                    onClick={() => { window.open(`https://instagram.com/${ig_ce}`) }} width="35px" height="35px" />
                                            </Item>
                                            <p className="w-100 text-center" style={{ fontSize: '16px' }}>
                                                Putri dari: <br />
                                                {bapak_ce}<br />
                                                & {ibu_ce}
                                            </p>
                                        </Col>
                                        <Col md={2} >
                                            <Row className="h-100">
                                                <h1 className="w-100 text-center my-auto"
                                                    style={{ fontFamily: 'Parisienne, cursive', color: orange }} data-aos="fade-left">
                                                    &
                                            </h1>
                                            </Row>
                                        </Col>
                                        <Col md={5}>
                                            <h1 className="w-100 text-center"
                                                data-aos="fade-right"
                                                style={{
                                                    fontFamily: 'Parisienne, cursive', color: orange
                                                }}>
                                                {lengkap_co}
                                            </h1>


                                            <Item>
                                                <img src={logoig} className='btn p-0 my-3'
                                                    onClick={() => { window.open(`https://instagram.com/${ig_co}`) }} width="35px" height="35px" />
                                            </Item>
                                            <p className="w-100 text-center" data-aos="fade-left"
                                                style={{ fontSize: '16px' }}>
                                                Putra dari: <br />{bapak_co}<br />
                                                & {ibu_co}
                                            </p>
                                        </Col>
                                    </Item>
                                </Container>

                                <Container fluid className="text-center px-4 dinny py-3"
                                    style={{ color: 'white' }} >
                                    <Item>
                                        <p className="fs16" style={{}} data-aos="zoom-in">
                                            Yang Insyaa Allah akan dilaksanakan pada:
                                        </p>
                                    </Item>
                                    <Item>
                                        <p className="fs20" style={{}} data-aos="fade-left">
                                            <b>
                                                SABTU <span className="fs36">06</span> MARET 2021
                      </b>
                                        </p>
                                    </Item>
                                    <Item>
                                        <p className="fs20 col-sm-4" style={{ color: 'white', }} data-aos="fade-right">
                                            <b style={{ color: orange }}>Akad Nikah </b><br />
                                            <span style={{fontSize:'16px'}}>
                                                <br />
                                            09.00 WIB - Selesai
                                             </span>
                                            {/* <br />
                      <span className="cblack fs12">
                        (Hanya dihadiri oleh keluarga)
                      </span> */}

                                        </p>
                                        <p className="px-3 d-none d-sm-block"
                                            style={{
                                                color: 'white', fontSize: '72px',
                                            }} data-aos="fade-down-right">
                                            \
                    </p>
                                        <div className="col-8 d-sm-none" style={{ borderBottom: `2px solid ${cmain}` }}>
                                        </div>
                                        <p className="fs20 pt-3 pt-sm-0 col-sm-4"
                                            style={{ color: 'white', }} data-aos="fade-down-left">
                                            <b style={{ color: orange }}>Resepsi</b><br />
                                            <span style={{fontSize:'16px'}}>
                                                <br />
                                                Sesi 1 : 11.30-12.30 <br/>
                                                (tamu mempelai wanita) 
                                                <br/>
                                                Sesi 2 : 12.30-14.00 <br/>
                                                (tamu mempelai pria)
                                                <br/>
                                                <i>
                                                diharapkan tamu datang sesuai pada waktu yg telah ditentukan untuk menghindari keramaian dalam acara
                                                </i>
                                             </span>


                                        </p>
                                    </Item>
                                    <Item>
                                        <p className="fs16 pt-3" style={{}} data-aos="fade--left">
                                            <b>Sari Kuring Indah (Joglo) 
                                            </b> <br />
                                            <span >
                                            Jl. Jend Ahmad Yani 35, Sukmajaya, Jombang, Cilegon
                    </span>
                                        </p>
                                    </Item>
                                    
                                    <Item>
                                        <Col xs={10} sm={3}
                                            style={{
                                                border: `2px solid white`,
                                                borderRadius: '10px'
                                            }}
                                            onClick={() => {
                                                window.open('https://goo.gl/maps/5pzkMjwGJLouEn6E6')
                                            }}
                                            data-aos="fade-right"
                                            className="p-2 mx-sm-2 btn">
                                            <Item>
                                                <img src="https://www.flaticon.com/svg/static/icons/svg/979/979874.svg" className="img-fluid"
                                                    style={{ width: "10%", height: '10%' }} />
                                                <p className="mb-0 my-auto ml-3" style={{ color: 'white' }}>
                                                    <b>Get Direction</b>
                                                </p>
                                            </Item>
                                        </Col>
                                        <Col
                                            onClick={() => window.open(gcalendar)}
                                            xs={10} sm={3}
                                            style={{
                                                border: `2px solid white`,
                                                borderRadius: '10px'
                                            }}
                                            data-aos="fade-left"

                                            className="p-2 mx-sm-2 mt-3 mt-sm-0 btn">
                                            <Item>
                                                <img src="https://www.flaticon.com/svg/static/icons/svg/979/979863.svg" className="img-fluid"
                                                    style={{ width: "10%", height: '10%' }} />
                                                <p className="mb-0 my-auto ml-3" style={{ color: 'white' }}>
                                                    <b>Add to Calendar</b>
                                                </p>
                                            </Item>
                                        </Col>
                                    </Item>
                                </Container>
                                <Container id='sectiongold57'>

                                    <div className='pt-3' data-aos="fade-right">

                                        <Item>
                                            <div data-aos="fade-left" data-aos-duration="1000"
                                                className='col-10 col-lg-8 kotak'
                                                style={{ backgroundColor: cmain }}>
                                                <Item>
                                                    <div className='item'>
                                                        <Item>
                                                            <div>
                                                                {days}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span>
                                                                Days
                      </span>
                                                        </Item>
                                                    </div>
                                                    <div className='dot'>:</div>
                                                    <div className='item'>
                                                        <Item>
                                                            <div>
                                                                {hours}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span>
                                                                Hours
                      </span>
                                                        </Item>
                                                    </div>
                                                    <div className='dot'>:</div>
                                                    <div className='item'>
                                                        <Item>
                                                            <div >
                                                                {minutes}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span>
                                                                Mins
                      </span>
                                                        </Item>
                                                    </div>
                                                    <div className='dot' >:</div>
                                                    <div className='item'>
                                                        <Item>
                                                            <div>
                                                                {seconds}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span>
                                                                Secs
                      </span>
                                                        </Item>
                                                    </div>
                                                </Item>

                                            </div>
                                        </Item>
                                    </div></Container>
                                

                                <Container className="py-3" data-aos="fade-right">
                                    <Item>
                                        <Col xs={12} md={6}>
                                            <img src={covid} className="w-100 img-fluid" />
                                        </Col>
                                    </Item>
                                </Container>
                                


                                <Container className='mt-3 py-3' data-aos="fade-right" data-aos-duration="1000">
                                    <Slider slide={slider} data-aos="fade-right" />
                                </Container>
                                <Container id='sectiongold56'>
                                    <div className='pt-3' data-aos="fade-left">

                                        <div data-aos={`fade-right`} data-aos-duration="2000">
                                            <Item>
                                                <div className='kotak col-10' style={{ backgroundColor: cmain }}>
                                                    <Item>
                                                        <p className='text-left p-2 px-4 ' style={{fontSize:'16px',fontFamily:'Poppins, sans-serif'}}>
                                                        Masa lalu, hari ini, dan masa depan adalah karunia yang telah Allah berikan kepada kita.<br/> 

<br/>The Past <br/>
Jika kalian mengetahui masa lalu kami maka kenanglah jika itu baik. Ambil hikmahnya jika itu  buruk.<br/>
<br/>Present <br/>
Jika kalian mengetahui cerita kami hari ini, maka syukurilah jika itu baik, koreksi kami jika itu buruk. <br/>
<br/>Our Future <br/>
Jika kalian mengetahui rencana baik kami dimasa depan, maka doakanlah kami sehingga itu bisa terwujud.<br/>
                                                            
</p>
                                                    </Item>
                                                </div>

                                            </Item>
                                        </div>
                                    </div>
                                </Container>

                                <Container id='sectiongold58' >

                                    <div className='pt-3 mt-4 mt-lg-5 mb-lg-3' data-aos="fade-right">
                                        <Item>
                                            <Col xs={4} lg={2}>
                                                <img data-aos="zoom-in" data-aos-duration="1000" src={bunga6} className='img-fluid w-100' />
                                            </Col>
                                        </Item>
                                        <Item>
                                            <div className='col-10 col-lg-6 kotak pb-4 pt-4' data-aos="left-left" data-aos-duration="1000">
                                                <Item>
                                                    <h1 className="w-100 text-center" style={{
                                                        fontFamily: '"Marck Script", cursive',
                                                        color: cmain
                                                    }}>
                                                        Doa Untuk Kedua Mempelai
                    </h1>
                                                </Item>
                                                <Item>
                                                    <form className="col-12 w-100">
                                                        <input ref={this.nama} type='text' className="col-12 w-100 text-center" placeholder="Name" name='nama' />
                                                        <input ref={this.pesan} type='text-area' className="col-12 w-100 text-center bigger" placeholder="Message" name='pesan' />
                                                        {
                                                        this.dropdown()
                                                    }
                                                    <Item>
                                                        <div id="formradio">
                                                            <div class="custom_radio row justify-content-center">
                                                                <div onClick={() => {
                                                                    this.setState({ hadir: true })
                                                                }
                                                                }>
                                                                    <input type="radio" id="featured-1" name="featured" checked={hadir ? true : false} />
                                                                    <label for="featured-1">Hadir</label>
                                                                </div>
                                                                <div onClick={() => {
                                                                    this.setState({ hadir: false })
                                                                }
                                                                } className="pl-5">
                                                                    <input type="radio" id="featured-2" name="featured" checked={hadir ? false : true} />
                                                                    <label for="featured-2"

                                                                    >Tidak Hadir</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Item>

                                                        <Item>
                                                            <Col xs={12} className=''>
                                                                {
                                                                    submitted == true ? (
                                                                        <Alert variant='success' style={{ fontSize: '16px' }}>
                                                                            Pesan anda sudah disampaikan
                                                                        </Alert>) : (submitted === false ? (
                                                                            <Alert variant='danger' style={{ fontSize: '16px' }}>
                                                                                {
                                                                                    err.map(val => {
                                                                                        return (
                                                                                            <li>{val}</li>
                                                                                        )
                                                                                    })
                                                                                }

                                                                            </Alert>
                                                                        ) : false)
                                                                }

                                                            </Col>
                                                        </Item>
                                                        <Item>
                                                            <div className='col-6 button rounded btn'
                                                                onClick={() => this.handleSubmit()} style={{ backgroundColor: cmain, color: 'white' }} no> Kirim </div>
                                                        </Item>
                                                    </form>
                                                </Item>
                                            </div>
                                        </Item>
                                    </div>
                                </Container>

                                <Foot ig={logoig} />
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    }
}

