import React, { Component } from 'react'
import { Helm } from '../components/header'
import Container from 'react-bootstrap/Container'
import Header, { Item, Mempelai, Divider, Slider } from '../components/main'
import { cap } from '../params'
import { Form, Row, Col, Alert } from 'react-bootstrap'
import music from '../assets/music/arif.aac'
import AOS from 'aos';
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from '../assets/img/logo.ico'
import '../style/arif.css'
import path from '../assets/img/arif/path1.svg'
import path3 from '../assets/img/adnan/path3.svg'
import arif from '../assets/img/arif/arif.webp'
import cincin from '../assets/img/arif/cincin.svg'
import logoig from '../assets/img/arif/logoig.svg'
import burung from '../assets/img/gold3/burung.svg'
import bg from '../assets/img/arif/modal.jpg'
import bg1 from '../assets/img/arif/bggrey.webp'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import post from '../params/post'
export default class Halo extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef()

        this.nama = React.createRef()
        this.alamat = React.createRef()
        this.pesan = React.createRef()

        this.state = {
            days: '00',
            minutes: '00',
            hours: '00',
            seconds: '00',
            hide: true,
            hadir: true,
            err: [],
            submitted: ''
        }
    }

    useQuery = () => {
        return new URLSearchParams(this.props.location.search);
    }

    componentDidMount() {
        AOS.init({
            // initialise with other settings
            duration: 1000
        });
        var countDownDate = new Date("12/12/2020").getTime();

        // Update the count down every 1 second
        var x = setInterval(() => {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            this.setState({
                days: days, hours: hours, minutes: minutes, seconds: seconds
            })
        }, 1000);

    }
    play = () => {
        AOS.refresh()
        var snd = new Audio(music);
        snd.type = 'audio/aac';
        snd.play();

        this.setState({ hide: false })
        setTimeout(() => {
            var elmnt = document.getElementById('top');
            elmnt.scrollIntoView();
        }, 1000)
    }
    googleCalendar = () => {
        window.open(`https://calendar.google.com/calendar/r/eventedit?
  text=Pernikahan%20Arif%20dan%20Devia
  &details=Pernikahan%20Arif%20dan%20Devia
  &dates=20201106T133000/20201106T160000
  &ctz=Indonesia%2FEast_Java&title=Pernikahan%20Arif%20dan%20Devia
      `, '_blank')
    }
    handleSubmit = () => {
        
        let local=localStorage.getItem('pesan')
        let err = []
        if (this.nama.current.value == "") {
            err.push('Nama tidak Boleh Kosong')
        }
        if (this.alamat.current.value == "") {
            err.push('Alamat tidak Boleh Kosong')
        }
        if (this.pesan.current.value == "") {
            err.push('Pesan tidak Boleh Kosong')
        }
        if (err.length == 0) {
            confirmAlert({
                message: local?`Kami mendeteksi bahwa anda telah mengirimkan pesan \" ${local} \", apakah anda ingin mengirim pesan lagi?`:'Yakin untuk Mengirim Pesan?',
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async() => {
                            let send = await post(
                                ` dari: "${this.nama.current.value}", hadir: "", jumlahhadir: "", kepada: "nita-fahrul", pesan:"${this.pesan.current.value}",alamat: ""`
                              )
                            if (send.status == 200) {
                                this.setState({ submitted: true })
                                localStorage.setItem('pesan', this.pesan.current.value)
                                this.nama.current.value = ''
                                this.pesan.current.value = ''
                            }else{
                                err.push('Koneksi Gagal')
                            }
                            
                        }
                    },
                    {
                        label: 'No',
                        onClick: () => {}
                    }
                ]
            });

        } else {
            this.setState({ err: err, submitted: false })
        }


    }

    render() {
        let query = this.useQuery().get('name');
        query = query ? cap(query) : ''
        let { days, hours, minutes, seconds, hide, hadir, err, submitted } = this.state

        return (
            <>
                <Helm
                    title='Undanganku - Nita & Fahrul'
                    desc="undangan digital berbasis website untuk berbagai kebutuhan acara"
                    logo={logo}
                />
                <div id='arif'>
                    <Container fluid id='g3-header' className='relative pl-0 pr-0' 
                    style={{ backgroundImage: `url(${bg})` }}>
                        <Item>
                            <Col xs={12} md={4} className='m-2 m-md-0 '>
                                <img className='img-fluid w-100 p-2' src={'https://i.ibb.co/wYcWXWx/Logo-3.png'} data-aos="fade-left" />
                            </Col>
                        </Item>
                        <Item>
                            {query ? (

                                <h2 className={`col-md-4 roboto-slab`}> Kepada : {query} </h2>
                            ) : false}
                        </Item>
                        <Row className='justify-content-center'>
                            <div onClick={() => { this.play() }}
                                className={`col-md-4 button roboto-slab btn ${hide ? 'show' : 'hide'}`}
                                style={{color:'white'}}>
                                Open
                            </div>
                        </Row>
                        <Col className={`absolute path ${hide ? 'hide' : 'show'}`} xs={12}>
                            <img className='img-fluid w-100' src={path} />
                        </Col>
                    </Container>
                    {!hide ? (
                        <>
                            <Container fluid>

                            </Container>
                            <Container fluid className='bg-green pt-3 pb-3' id='top'>
                                <Item>
                                    <Col xs={4} md={2} className='p-3 p-md-5'>
                                        <img src={cincin} className='img-fluid w-100' data-aos="fade-left" />
                                    </Col>
                                </Item>
                                <Item>
                                    <Col xs={12} md={8}>
                                        <p className='italic roboto-slab c-main' data-aos="fade-right">
                                            بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم
                                    <br /><br />
                                    وَمِنْ ءَايَٰتِهِۦٓ أَنْ خَلَقَ لَكُم مِّنْ أَنفُسِكُمْ أَزْوَٰجًا لِّتَسْكُنُوٓا۟ إِلَيْهَا وَجَعَلَ بَيْنَكُم مَّوَدَّةً وَرَحْمَةً ۚ إِنَّ فِى ذَٰلِكَ لَءَايَٰتٍ لِّقَوْمٍ يَتَفَكَّرُونَ
                                <br />
                                        </p>
                                        <p className='italic roboto-slab c-main f-hu' data-aos="fade-right">
                                            “Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu isteri-isteri dari jenismu sendiri, supaya kamu cenderung dan merasa tenteram kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berfikir.” <br /><br />(Ar-Rum: 21)
                            </p>
                                    </Col>
                                </Item>
                            </Container>
                            <div className='bgnew' style={{
                                backgroundImage:`url(${bg1})`,
                            }}>

                            
                            <Container id='pasangan'>

                                <Item>
                                    <div className='p-3' id="pasangan">
                                        <Item>
                                            <Col xs={12} md={10} className='p-3 rounded' data-aos="fade-left">
                                                <p className='c-grey tinos'>
                                                    Maha Suci Allah yang telah menciptakan Makhluk-Nya secara Berpasang-pasangan.
                                    <br></br><br></br>
                                    Ya Allah Perkenankan kami merangkai kasih sayang yang kau ciptakan diantara Putra Putri Kami
                                    </p>
                                    <Item>
                                                    <h1 className='c-grey lobster p-3 w-100 text-center'>
                                                        The Bride
                                                    </h1>
                                                    
                                                </Item>

                                                <Item>
                                                    <h1 className='c-main garamond p-3 w-100 text-center'>
                                                    Nita slavia sihite S.H

                                        </h1>
                                        <img src={logoig} 
                                                    onClick={()=>{
                                                        window.open('https://instagram.com/nitaslaviahhh')
                                                    }}
                                                    className="btn p-0"
                                                    style={{width:'50px',height:'50px'}}/>
                                                </Item>
                                                <div id="pasangan">
                                                    <Item>
                                                        <p className='s-bold tinos'>Putri ketiga dari:
</p>
                                                    </Item>
                                                    <Item>
                                                        <p className='tinos ortu'>

                                                            Bapak Muhammad Ali Sihite S.H<br />
                                            dan<br />
                                            Ibu Rosna Dewi Nasution
                                        </p>
                                                    </Item>
                                                </div>
                                            

                                                <Item>
                                                    <h1 className='c-main lobster p-3 larger'>
                                                        &
                                                    </h1>
                                                </Item>
                                                
                                                <Item>
                                                    <h1 className='c-grey lobster p-3 w-100 text-center'>
                                                        The Groom
                                                    </h1>
                                                </Item>

                                                <Item>
                                                    <h1 className='c-main garamond p-3 w-100 text-center'>
                                                    Ahmad fakhrurozi S.kom
                                                    </h1>
                                                    <img src={logoig} 
                                                    className="btn p-0"
                                                    onClick={()=>{
                                                        window.open('https://instagram.com/afahrulaf')
                                                    }}
                                                    style={{width:'50px',height:'50px'}}/>

                                                </Item>
                                                <div id="pasangan">
                                                    <Item>
                                                        <p className=' s-bold tinos'>Putra pertama Dari :</p>
                                                    </Item>
                                                    <Item>
                                                        <p className='tinos'>
                                                        Bapak H. Agus firdaus <br />
                                                        dan<br />
                                                        Ibu Ami indrawati
                                                        </p>
                                                    </Item>
                                                </div>
                                                
                                                   
                                                
                                            </Col>
                                        </Item>
                                    </div>
                                </Item>
                            </Container>
                            <Container fluid className=' p-4' id='save'>
                                {/* <h1 className='sacramento s-bold c-main' data-aos="fade-right">
                                    Save The Date
                                </h1> */}
                                <Item>
                                    <Col xs={10} md={6} className='p-3 rounded  s-bold c-main' data-aos="fade-left">
                                        <Item>
                                            <div className='item'>
                                                <Item>
                                                    <div>
                                                        {days}
                                                    </div>
                                                </Item>
                                                <Item>
                                                    <span>
                                                        Days
                                        </span>
                                                </Item>
                                            </div>
                                            <div className='dot'>:</div>
                                            <div className='item'>
                                                <Item>
                                                    <div>
                                                        {hours}
                                                    </div>
                                                </Item>
                                                <Item>
                                                    <span>
                                                        Hours
                                    </span>
                                                </Item>
                                            </div>
                                            <div className='dot'>:</div>
                                            <div className='item'>
                                                <Item>
                                                    <div >
                                                        {minutes}
                                                    </div>
                                                </Item>
                                                <Item>
                                                    <span>
                                                        Mins
                                        </span>
                                                </Item>
                                            </div>
                                            <div className='dot' >:</div>
                                            <div className='item'>
                                                <Item>
                                                    <div>
                                                        {seconds}
                                                    </div>
                                                </Item>
                                                <Item>
                                                    <span>
                                                        Secs
                                            </span>
                                                </Item>
                                            </div>
                                        </Item>
                                    </Col>
                                </Item>
                                <Item>
                                    <Col xs={12} md={8} className='p-3 border-pink rounded'>
                                        <Item>
                                            <Col xs={4} md={2} className='p-3'>
                                                <img src={burung} className='img-fluid w-100' data-aos="fade-right" />
                                            </Col>
                                        </Item>
                                        <h1 className='sacramento s-bold mt-3 c-main f-mid' data-aos="fade-left">
                                            Akad Nikah
                                        </h1>
                                        <p className='f-small poppins mt-4' data-aos="fade-left">
                                            <div className='s-bold '>
                                                Sabtu, 12 Desmber 2020
                                            </div>
                                            <span className='m-2'>07.00 WIB - Selesai</span><br />
                                        </p>
                                 <h1 className='sacramento s-bold mt-3 c-main f-mid' data-aos="fade-left">
                                            Resepsi
                                        </h1>
                                        <p className='f-small poppins mt-4' data-aos="fade-left">
                                            <div className='s-bold '>
                                                Sabtu, 12 Desmber 2020<br/><br/>
                                            </div>
                                            <span className='m-2'>
                                            Sesi 1 : 10.00 - 12.00 <br/>
                                            (rekan2 kedua mempelai)<br/><br/>
                                             Sesi 2 : 12.00 - 15.30 <br/>
                                             (saudara dan rekan2 kedua orang tua mempelai)
                                            </span><br/><br/>

                                            <b>Aula tri bakti yudha</b> <br />
                                             Komplek asrama yon bek ang 1 kostrad jalan baruda cibinong bogor
                                            Cibinong, Kab. Bogor, Jawa Barat, 16911
                                         </p>
                                        <Item>
                                            <Col onClick={() => { window.open('https://goo.gl/maps/fpLNAjAqGpbTSkif7') }} xs={8} md={4} className='button btn poppins c-white f-small rounded p-1' 
                                             data-aos="zoom-in">
                                                Get Direction
                                        </Col>
                                        </Item>
                                        <Item>
                                            <Col 
                                                onClick={() => {
                                                    window.open('https://calendar.google.com/event?action=TEMPLATE&tmeid=NDR2ODVuYjNiNWZ2dTJycDU5MW0yZTM5bGMgYXJpZWZjNzJAbQ&tmsrc=ariefc72%40gmail.com')
                                                }}
                                                xs={8} md={4} className='button btn poppins c-white f-small rounded p-1 mt-3 btn'data-aos="zoom-in">
                                                Add to Google Calendar
                                            </Col>

                                        </Item>
                                    </Col>
                                </Item>
                                <Item>
                                    <Col xs={12} md={8} >
                                        <Alert variant='warning' className='f-14  mt-3 text-justify pl-0'>
                                            <p>
                                                <ol>
                                                    <li>Sehubungan dengan pandemi covid 19 saat ini  mohon maaf kami harus mengikuti aturan pemerintah dengan membatasi jumlah tamu undangan yang hadir untuk tidak lebih dari 2 orang</li>
                                                    <li>Anak anak dibawah usia 10 tahun dan lansia 70 tahun dihimbau agar tidak masuk ke area ballroom (kecuali keluarga)</li>
                                                    <li>Tamu wajib mengikuti protokol kesehatan</li>
                                                </ol>

                                            </p>
                                        </Alert>
                                    </Col>
                                </Item>



                            </Container >
                            <Container className='bg-green mx' fluid id='form'>
                                <Item>

                                    <Col xs={10} md={6} className='bg-white rounded poppins mt-3 pt-4 pb-4 ' data-aos="zoom-in">
                                        <h1 className='sacramento c-main f-small send'>
                                            Kirim Ucapan
                                        </h1>
                                        <form className="col-12 w-100 adnan">
                                            <input ref={this.nama} type='text' className="col-12 w-100 text-center" placeholder="Nama" name='nama' defaultValue={query ? query : ''} />
                                            <input ref={this.alamat} type='text' className="col-12 w-100 text-center" placeholder="Alamat" name='alamat' />
                                            <input ref={this.pesan} type='text-area' className="col-12 w-100 text-center bigger" placeholder="Pesan" name='pesan' />
                                                
                                            <Item>
                                                <Col xs={12} className=''>
                                                    {
                                                        submitted == true ? (
                                                            <Alert variant='success'>
                                                                Pesan anda sudah disampaikan
                                                            </Alert>) : (submitted === false ? (
                                                                <Alert variant='danger'>
                                                                    {
                                                                        err.map(val => {
                                                                            return (
                                                                                <li>{val}</li>
                                                                            )
                                                                        })
                                                                    }

                                                                </Alert>
                                                            ) : false)
                                                    }

                                                </Col>
                                            </Item>
                                            <Item>
                                                <div  className='col-6 button rounded btn' onClick={() => this.handleSubmit()}> Kirim </div>
                                            </Item>
                                        </form>

                                    </Col>
                                </Item>

                                <Item>
                                    <h1 className='sacramento c-main f-small send pt-3 pb-3'>
                                        Nita & Fahrul
                        </h1>
                                </Item>
                            </Container>
                            </div>
                            <Container fluid className='bg-green relative p-0' id='path'>
                                <img className='absolute img-fluid w-100 mx mr-0' src={path3} />
                            </Container>
                            <Container fluid className='bg-white poppins pb-5' id='footer'>
                                <Item>
                                    <h1 data-aos-duration="1000">
                                        Possible Wedding
              </h1>
                                </Item>
                                <Item>
                                    <h2 data-aos-duration="1000">
                                        Digital Invitation
              </h2>
                                </Item>
                                <Item>
                                    <div className='col-3 col-md-1 p-md-4 btn' 
                                        onClick={() => {
                                            window.open('http://instagram.com/possiblewedding')
                                        }}>
                                        <img src={logoig} className='img-fluid w-100'
                                        />
                                    </div>
                                </Item>
                            </Container>
                        </>
                    ) : false}
                </div>
            </>
        )
    }
}