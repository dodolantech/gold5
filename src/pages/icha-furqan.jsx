import React, { Component } from 'react' // Import
import Container from 'react-bootstrap/Container'
import { Item, Float, Foot, Slider } from '../components/main'
import { Helm } from '../components/header'
import { Row, Col, Alert } from 'react-bootstrap'
import { cap, pw } from '../params'
import '../style/style.css'
import '../style/sty.scss'
import '../style/montserrat.scss'
import music from '../assets/music/ninda.mp3'
import logoig from '../assets/img/dinny/logoig.svg'
import bunga6 from '../assets/img/bunga6.png'
import AOS from 'aos';
import { gambar } from '../params'
import post from '../params/post'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import logo from '../assets/img/logo.ico'
import pathputih1 from '../assets/img/ninda/pathputih1.svg'
import pathblue1 from '../assets/img/ninda/pathblue1.svg'
import covid from '../assets/img/ninda/covid.svg'

import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';

let cmain = '#EFE6DD'
let font = '#5D3B19'
let black = 'rgb(38,38,38)'

let id = 'icha-furqan'
let inisial_co = 'Furqan'
let inisial_ce = 'Icha'
let lengkap_co = 'Furqanuddin, S.ST., M.Sc.'
let lengkap_ce = 'Shabrina Nashya Aswin, S.TP.'
let bapak_co = 'Bapak Drs. Ridhwan Hajjaj, M.A.'
let ibu_co = 'Ibu Nurul Fuad A. Binsih'
let bapak_ce = "Bapak Ir. H. St. Rainal Aswin Darab"
let ibu_ce = "Ibu dr. Hj. Herlina Zahar"

let foto_ce = "https://i.ibb.co/WHrgx9M/Individu-2.jpg"
let foto_co = "https://i.ibb.co/wRYZWc4/Individu-1.jpg"
let waktunikah = "02/06/2021"

let modal = "https://i.ibb.co/d0KHHtr/Modal-Baru-Fix.jpg"
let openlogo = pw(id, "logo.png")

let gmaps = "https://goo.gl/maps/Zc1LcikijUBBDBi78"
let gcalendar = 'https://calendar.google.com/event?action=TEMPLATE&tmeid=MmRjMGppZ3BjMTRucXY5dHAxczJzdG1rcjMgYXJpZWZjNzJAbQ&tmsrc=ariefc72%40gmail.com'

let slide = ["	https://i.ibb.co/ZW2MT9h/DSC08575-01.jpg	",
    "	https://i.ibb.co/DzLf4XX/DSC08628-01.jpg	",
    "	https://i.ibb.co/1fxd44Z/DSC08662-01.jpg	",
    "	https://i.ibb.co/Tqkqsm7/DSC08721-01.jpg	",
    "	https://i.ibb.co/FzZJSRB/DSC08724-01.jpg	",
]
let blue = '#5e7561'

export default class Halo extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef()
        this.myRef = React.createRef()
        this.nama = React.createRef()
        this.alamat = React.createRef()
        this.pesan = React.createRef()

        this.state = {
            acara: [],
            days: '00',
            minutes: '00',
            hours: '00',
            seconds: '00',
            hide: true,
            hadir: true,
            err: [],
            submitted: '',
            sesi: 2
        }
    }

    componentDidMount() {
        AOS.init({
            // initialise with other settings
            duration: 2000
        });
        var countDownDate = new Date(waktunikah).getTime();

        // Update the count down every 1 second
        var x = setInterval(() => {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            this.setState({
                days: days, hours: hours, minutes: minutes, seconds: seconds
            })


        }, 1000);

    }

    play = () => {
        AOS.refresh()
        var snd = new Audio(pw(id,"music.mp3"));
        snd.type = 'audio/mp3';
        snd.play();
        this.setState({ hide: false })
        setTimeout(() => {
            var elmnt = document.getElementById('top');
            elmnt.scrollIntoView();
        }, 1000)
    }

    useQuery = () => {
        return new URLSearchParams(this.props.location.search);
    }
    handleSubmit = async () => {
        let err = []
        let local = localStorage.getItem('pesan')
        if (this.nama.current.value == "") {
            err.push('Nama tidak Boleh Kosong')
        }
        if (this.pesan.current.value == "") {
            err.push('Pesan tidak Boleh Kosong')
        }
        if (err.length == 0) {
            confirmAlert({
                message: local ? `Kami mendeteksi bahwa anda telah mengirimkan pesan \" ${local} \", apakah anda ingin mengirim pesan lagi?` : 'Yakin untuk Mengirim Pesan?',
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async () => {
                            let send = await post(
                                ` dari: "${this.nama.current.value}", hadir: "", jumlahhadir: "", kepada: "${id}", pesan:"${this.pesan.current.value}",alamat: ""`
                              )
                            if (send.status == 200) {
                                this.setState({ submitted: true })
                                localStorage.setItem('pesan', this.pesan.current.value)
                                this.nama.current.value = ''
                                this.pesan.current.value = ''
                            }else{
                                err.push('Koneksi Gagal')
                            }
                        }
                    },
                    {
                        label: 'No',
                        onClick: () => { }
                    }
                ]
            });
        } else {
            this.setState({ err: err, submitted: false })
        }


    }
    render() {
        let { hadir, days, hours, minutes, seconds, hide, submitted, err, sesi } = this.state
        let slider = []
        slide.map(v => {
            slider.push(gambar(v, 95, 'auto&func=fit&bg_img_fit=1&bg_opacity=0.75&w=800&h=520'))
        })
        let query = this.useQuery().get('u');
        query = query ? cap(query) : ''

        return (
            <>
                <Helm
                    title={`Undanganku - ${inisial_ce} & ${inisial_co}`}
                    desc="undangan digital berbasis website untuk berbagai kebutuhan acara"
                    logo={logo}
                    img={slide[0]}
                    url={`https://undanganku.me/${id}`}
                />

                <div id='gold5'>
                    {
                        this.useQuery().get('x') == "x" ? (<Float />) : false
                    }
                    <div className='w-100' style={{
                        overflow: 'hidden', maxWidth: '100vw',
                        backgroundColor: 'transparent'
                    }}>
                        <Container fluid id='g3-header' className='position-relative' style={{
                            backgroundImage: 'none',
                            backgroundColor: cmain
                        }}>
                            <div className="position-absolute" style={{ left: 0, bottom: '4%', width: '100vw' }}>
                                <Item>
                                    <p className="w-100 text-center" style={{color:font}}>Walimatul 'Urs</p>
                                    <Col xs={12} md={4} className='m-2 m-md-0 ' style={{marginBottom:'-50px'}}>
                                        <img className='img-fluid w-100 p-2'
                                            src={openlogo} data-aos="fade-left" data-aos="fade-left" />
                                    </Col>
                                </Item>
                                <Item>
                                    <p style={{fontFamily:'Tinos,serif',fontSize:'18px',color:font}} className="text-center">
                                        KEPADA YTH.<br/>
                                       
                                        <br/>
                                        <br/>
                                        <div style={{transform:'scale(1.4)'}}>
                                        {query ? query : ''}
                                        </div>
                                        <br/>
                                        
                                    </p>
                                </Item>
                                
                                <Row className='justify-content-center'>
                                    <div onClick={() => { this.play() }}
                                        // data-aos="fade-left"
                                        className={`col-md-4 button btn roboto-slab text-center ${hide ? 'show' : 'hide'}`}
                                        style={{ marginTop: 0, color: 'white', borderColor: font,fontFamily:'Tinos,serif',backgroundColor:font }}>
                                        Open Invitation
                                    </div>
                                </Row>

                            </div>
                            {/* <img className="w-100 img-fluid position-absolute" style={{ left: 0, bottom: 0 }}
                                src={pathblue1}
                            /> */}

                        </Container>

                        <div className={hide ? 'd-none' : 'd-block'}>
                            <div id="top" style={{ backgroundColor: 'white',backgroundImage:`url('https://vectorforfree.com/wp-content/uploads/2019/04/Leaf_Pattern_VectorForFree_02.jpg')`,
                            backgroundSize:'25%' }}>
                                <Container fluid style={{
                                    backgroundColor: cmain
                                }}>
                                    <Container className="dinny px-3 py-5 " >
                                        <Item>

                                            
                                            <p className="text-center  px-3 py-3 w-100 text-center"
                                                style={{
                                                    color: font,
                                                    fontFamily: 'Montserrat,Helvetica,Arial,Lucida,sans-serif',
                                                    fontSize: '14px'

                                                }} data-aos="fade-left">
                                               "Dan diantara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir" 
<br/><br/>
(Q.S Ar-Rum: 21)
                                        </p>
                                        </Item>

                                    </Container>
                                </Container>
                                <Container fluid style={{ backgroundColor: cmain, padding: 0, position: 'relative' }}>
                                    <img src={pathputih1} className="position-absolute"
                                        style={{ bottom: 0, left: 0, width: '110vw' }} />
                                </Container>
                                <Container className="py-3" >
                                    <Item>
                                        <Col xs={10} md={4}>
                                            <img src={pw(id, "bismillah-black.svg")} className="img-fluid w-100" />
                                        </Col>
                                        <p className="fs16 text-center  px-3 py-3 w-100 text-center"
                                            style={{ color: '#171717', fontSize: '16px' }} data-aos="fade-left">
                                            Assalamu'alaikum Warahmatullahi  Wabarakatuh<br /><br />
                                            Dengan memohon rahmat dan ridha Allah SWT,<br />
                                            kami bermaksud menyelenggarakan pernikahan putra-putri kami
                                        </p>
                                    </Item>
                                </Container>
                                <Container style={{ color: 'black' }} className="py-5">
                                    <Item>
                                        <Col md={5}>
                                            <h1 className="w-100 text-left"
                                                style={{
                                                    fontFamily: '"Marck Script", cursive',
                                                    color: font,
                                                    fontSize: '1em',
                                                    fontWeight:'700'
                                                }} data-aos="fade-left">
                                                {lengkap_ce}
                                            </h1>
                                            {/* <img src={logoig} className='btn p-0 my-3'
                                                data-aos="fade-right"
                                                onClick={() => { window.open(`https://instagram.com/${ig_ce}`) }} width="35px" height="35px" /> */}

                                            <p className="w-100 text-left" style={{ fontSize: '14px' }}>
                                                <b>Putri dari: </b><br />
                                                {bapak_ce}<br />
                                                & {ibu_ce}
                                            </p>
                                        </Col>
                                        <Col md={2} >
                                            <Row className="h-100">
                                                <h1 className="w-100 text-center my-auto"
                                                    style={{
                                                        fontFamily: '"Marck Script", cursive',
                                                        color: cmain, fontSize: '72px'
                                                    }} data-aos="fade-left">
                                                    &
                                            </h1>
                                            </Row>
                                        </Col>
                                        <Col md={5}>
                                            <h1 className="w-100 text-right"
                                                data-aos="fade-right"
                                                style={{
                                                    fontFamily: '"Marck Script", cursive', color: font,
                                                    fontSize: '1em',fontWeight:'700'
                                                }}>
                                                {lengkap_co}
                                            </h1>

                                            {/* <div className="text-right">


                                                <img src={logoig} className='btn p-0 my-3'
                                                    onClick={() => { window.open(`https://instagram.com/${ig_co}`) }} width="35px" height="35px" />
                                            </div> */}
                                            <p className="w-100 text-right" data-aos="fade-left"
                                                style={{ fontSize: '14px' }}>
                                                <b>Putra dari </b><br />{bapak_co}<br />
                                                & {ibu_co}
                                            </p>
                                        </Col>
                                    </Item>
                                </Container>

                                <Container fluid className="text-center px-4 dinny py-3"
                                    style={{ color: 'black' }} >
                                    <Item>
                                        <p style={{ fontSize: '16px' }} data-aos="zoom-in">
                                            Yang Insyaallah akan dilaksanakan pada:
                                        </p>
                                    </Item>

                                    <Item>
                                        <Col md={5}>
                                            <p className="w-100 text-center" style={{ color: 'black', fontSize: '18px' }} data-aos="fade-right">
                                                <b style={{ color: font, fontSize: '36px', fontFamily: 'Sacramento, cursive' }}>
                                                   <br />Akad Nikah </b><br />
                                                <span style={{ fontSize: '16px' }}>

                                                    Jumat, 05 Februari 2021<br />
                                                    08.00-10.00 WIB
                                             </span>

                                            </p>
                                            <Item>
                                        <p style={{ fontSize: '16px' }} data-aos="fade--left">
                                            <b>Masjid Darul Ma'arif 

                                            </b> <br />
                                            Kel. Pondok II, Kec. Pariaman Tengah, Kota Pariaman
                                            <span >
                                                <br/>
                                        </span>
                                        </p>
                                    </Item>
                                        </Col>
                                        <Col md={5} className="pt-3 pt-md-0">
                                            <p className=" w-100 text-center" style={{ color: 'black', fontSize: '18px' }} data-aos="fade-right">
                                                <b style={{ color: font, fontSize: '36px', fontFamily: 'Sacramento, cursive' }}
                                                >
                                                    <br/>
                                                    Resepsi</b><br />
                                                <span style={{ fontSize: '16px' }}>
                                                Jumat-Sabtu, 05-06 Februari 2021<br />
                                                10.00-18.00 WIB
                                             </span>
                                            </p>
                                            <Item>
                                        <p style={{ fontSize: '16px' }} data-aos="fade--left">
                                            <b>Hotel Shafira 
                                            </b> <br />
                                            <span >
                                            Kel. Pondok II, Kec. Pariaman Tengah, Kota Pariaman
                                        </span>
                                        </p>
                                    </Item>
                                        </Col>
                                    </Item>
                                    
                                    <Item>
                                        <Col xs={10} sm={3}
                                            style={{
                                                border: `2px solid ${font}`,
                                                borderRadius: '10px'
                                            }}
                                            onClick={() => {
                                                window.open(gmaps)
                                            }}
                                            data-aos="fade-right"
                                            className="p-2 mx-sm-2 btn">
                                            <Item>
                                                <img src="https://www.flaticon.com/svg/static/icons/svg/979/979874.svg" className="img-fluid"
                                                    style={{ width: "10%", height: '10%' }} />
                                                <p className="mb-0 my-auto ml-3" style={{ color: font }}>
                                                    <b>Get Direction</b>
                                                </p>
                                            </Item>
                                        </Col>
                                        <Col
                                            onClick={() => window.open(gcalendar)}
                                            xs={10} sm={3}
                                            style={{
                                                border: `2px solid ${font}`,
                                                borderRadius: '10px'
                                            }}
                                            data-aos="fade-left"
                                            className="p-2 mx-sm-2 mt-3 mt-sm-0 btn">
                                            <Item>
                                                <img src="https://www.flaticon.com/svg/static/icons/svg/979/979863.svg" className="img-fluid"
                                                    style={{ width: "10%", height: '10%' }} />
                                                <p className="mb-0 my-auto ml-3" style={{ color: font }}>
                                                    <b>Add to Calendar</b>
                                                </p>
                                            </Item>
                                        </Col>
                                    </Item>
                                </Container>

                                <Container className="py-3">
                                    <Item>
                                        <Col xs={12} md={6}>
                                            <img src={pw(id,"covid.svg")} className="w-100 img-fluid" />
                                        </Col>
                                    </Item>
                                </Container>

                                <Container id='sectiongold57'>
                                    <h1 className="w-100 text-center" style={{
                                        fontFamily: '"Marck Script", cursive',
                                        color: font,
                                        fontSize: '36px'
                                    }}>
                                        Save The Date
                                    </h1>
                                    <div data-aos="fade-right">

                                        <Item>
                                            <div data-aos="fade-left" data-aos-duration="1000"
                                                className='col-10 col-lg-8 kotak'
                                                style={{ backgroundColor: cmain }}>
                                                <Item>
                                                    <div className='item'>
                                                        <Item>
                                                            <div style={{color:font}}>
                                                                {days}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span style={{color:font}}>
                                                                Days
                                                        </span>
                                                        </Item>
                                                    </div>
                                                    <div className='dot' style={{color:font}}>:</div>
                                                    <div className='item'>
                                                        <Item>
                                                            <div style={{color:font}}>
                                                                {hours}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span style={{color:font}}>
                                                                Hours
                                                    </span>
                                                        </Item>
                                                    </div>
                                                    <div className='dot' style={{color:font}}>:</div>
                                                    <div className='item'>
                                                        <Item>
                                                            <div style={{color:font}}>
                                                                {minutes}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span style={{color:font}}>
                                                                Mins
                                                    </span>
                                                        </Item>
                                                    </div>
                                                    <div className='dot' style={{color:font}}>:</div>
                                                    <div className='item'>
                                                        <Item>
                                                            <div style={{color:font}}>
                                                                {seconds}
                                                            </div>
                                                        </Item>
                                                        <Item>
                                                            <span style={{color:font}}>
                                                                Secs
                      </span>
                                                        </Item>
                                                    </div>
                                                </Item>

                                            </div>
                                        </Item>
                                    </div></Container>

                                <Container id='sectiongold56'>
                                    <div className='pt-3' data-aos="fade-left">

                                        <div data-aos={`fade-right`} data-aos-duration="2000">
                                            <Item>
                                                <div className='kotak col-12' style={{ backgroundColor: cmain }}>
                                                    <Item>
                                                        <p className='text-center p-2 px-4 ' style={{ fontSize: '16px',color:font }}>
                                                        Merupakan suatu kehormatan apabila Bapak/Ibu/Saudara/i berkenan hadir memberikan doa. Atas kehadiran dan doa terbaik dari semuanya, kami ucapkan terima kasih. 
<br/><br/>
Wassalamu'alaikum Warahmatullahi Wabarakatuh
<br/><br/>
Kami yang berbahagia, 

</p>
<Row className="w-100">
<p className='text-center pl-2 pr-1 col-6' style={{ fontSize: '12px',color:font }}>
Keluarga Besar: <br/> 
Ir. H. St. Rainal Aswin<br/>
dr. Hj. Herlina Zahar

</p>
<p className='text-center pr-2 pl-1 col-6' style={{ fontSize: '12px',color:font }}>

Keluarga Besar: <br/> 
Drs. Ridhwan Hajjaj, M.A. <br/>
Nurul Fuad A. Binsih

</p>
</Row>
                                                    </Item>
                                                </div>

                                            </Item>
                                        </div>
                                    </div>
                                </Container>                               

                                <Container fluid style={{ backgroundColor: cmain }} className="pb-3">
                                    <Container id='sectiongold58' >

                                        <div className='pt-3 mt-4 mt-lg-5 mb-lg-3' data-aos="fade-right">
                                            <Item>
                                                <Col xs={4} lg={2}>
                                                    <img data-aos="zoom-in" data-aos-duration="1000" src={bunga6} className='img-fluid w-100' />
                                                </Col>
                                            </Item>
                                            <Item>
                                                <div className='col-10 col-lg-6 kotak pb-4 pt-4' data-aos="left-left" data-aos-duration="1000">
                                                    <Item>
                                                        <h1 className="w-100 text-center" style={{
                                                            fontFamily: '"Marck Script", cursive',
                                                            color: font
                                                        }}>
                                                            Send Your Wishes
</h1>
                                                    </Item>
                                                    <Item>
                                                        <form className="col-12 w-100">
                                                            <input ref={this.nama} type='text' className="col-12 w-100 text-center" placeholder="Name" name='nama' />
                                                            <input ref={this.pesan} type='text-area' className="col-12 w-100 text-center bigger" placeholder="Message" name='pesan' />
                                                            <Item>
                                                                <Col xs={12} className=''>
                                                                    {
                                                                        submitted == true ? (
                                                                            <Alert variant='success' style={{ fontSize: '16px' }}>
                                                                                Pesan anda sudah disampaikan
                                                                            </Alert>) : (submitted === false ? (
                                                                                <Alert variant='danger' style={{ fontSize: '16px' }}>
                                                                                    {
                                                                                        err.map(val => {
                                                                                            return (
                                                                                                <li>{val}</li>
                                                                                            )
                                                                                        })
                                                                                    }

                                                                                </Alert>
                                                                            ) : false)
                                                                    }

                                                                </Col>
                                                            </Item>
                                                            <Item>
                                                                <div className='col-6 button rounded btn'
                                                                    onClick={() => this.handleSubmit()} style={{ backgroundColor: font, color: 'white' }} no> Kirim </div>
                                                            </Item>
                                                        </form>
                                                    </Item>
                                                </div>
                                            </Item>
                                        </div>
                                    </Container>
                                </Container>
                                <Foot ig={logoig} dark />
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    }
}

